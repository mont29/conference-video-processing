# SPDX-License-Identifier: GPL-2.0-or-later

from . import (
    settings,
    stages,
)
if "bpy" in locals():
    import importlib
    if "settings" in locals():
        importlib.reload(settings)
    if "stages" in locals():
        importlib.reload(stages)


import bpy
from bpy.props import (
        StringProperty,
        BoolProperty,
        FloatProperty,
        IntProperty,
        EnumProperty,
        CollectionProperty,
        PointerProperty,
        )


import time


IS_DEBUG = settings.IS_DEBUG


class BConVideoPanel:
    bl_space_type = 'SEQUENCE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "BCon Video"

    @classmethod
    def poll(cls, context):
        return hasattr(context.scene, "bcon_video_data")


class BCONVIDEO_MT_operations(bpy.types.Menu):
    """Advanced or uncommon operations for the BCon Video tool"""
    bl_idname = "BCONVIDEO_MT_operations"
    bl_label = "Bcon Video Operations"

    def draw(self, context):
        layout = self.layout
        layout.label(text="WARNING: This menu should not be needed during production, only for initial set-up")
        layout.prop(context.scene.bcon_video_data.paths, "show_paths")
        layout.separator()
        layout.operator("sequencer.bcon_video_reset")
        layout.separator()
        layout.operator("sequencer.bcon_video_install_dependencies")


class BCONVIDEO_PT_paths(BConVideoPanel, bpy.types.Panel):
    bl_label = "Paths"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False  # No animation.

        row = layout.row()
        row.operator("sequencer.bcon_video_reload")
        row.menu("BCONVIDEO_MT_operations", text="", icon='SETTINGS')

        paths = context.scene.bcon_video_data.paths
        if paths.show_paths:
            layout.prop(paths, "reference_path")
            layout.prop(paths, "persistent_path")

            sub = layout.row()
            sub.prop(paths, "backup_dir")
            # ~ sub.prop(paths, "backup_dir_space", text="", slider=True)

            sub = layout.split(factor=0.8, align=True)
            sub.prop(paths, "raw_videos_dir")
            sub.prop(paths, "raw_videos_dir_space", text="", slider=True)


class BCONVIDEO_UL_talks(bpy.types.UIList):
    FILTER_CATEGORY_ALL = 'ALL'
    FILTER_CATEGORY_CURRENT = 'CURRENT'
    FILTER_CATEGORY_ACTIVE = 'ACTIVE'
    FILTER_CATEGORY_TODAY = 'TODAY'
    FILTER_CATEGORY_ERROR = 'ERROR'

    FILTER_LOCATION_ALL = 'ALL'
    FILTER_LOCATION_THEATER = 'theater'
    FILTER_LOCATION_STUDIO = 'studio'
    FILTER_LOCATION_CLASSROOM = 'classroom'
    FILTER_LOCATION_ATTIC = 'attic'

    enum_filter_category = (
        (FILTER_CATEGORY_ALL, "All", "Show all talks (no filtering)", 'ICON_NONE', 0),
        (FILTER_CATEGORY_CURRENT, "Current", "Show talks that are, or should be active currently (based on time)", 'ICON_NONE', 10),
        (FILTER_CATEGORY_ACTIVE, "Active", "Show talks that are currently being processed", 'ICON_NONE', 20),
        (FILTER_CATEGORY_TODAY, "Today", "Show all talks for today", 'ICON_NONE', 30),
        (FILTER_CATEGORY_ERROR, "Error", "Show all talks that have an error status", 'ICON_NONE', 40),
    )
    filter_category: EnumProperty(
        items=enum_filter_category,
        name="Filter by Category",
        description="Select which talks to show by category",
        default='ALL',
    )

    enum_filter_location = (
        (FILTER_LOCATION_ALL, "All", "", 'ICON_NONE', 0),
        (FILTER_LOCATION_THEATER, "Theater", "", 'ICON_NONE', 10),
        (FILTER_LOCATION_STUDIO, "Studio", "", 'ICON_NONE', 20),
        (FILTER_LOCATION_CLASSROOM, "Classroom", "", 'ICON_NONE', 30),
        (FILTER_LOCATION_ATTIC, "Attic", "", 'ICON_NONE', 40),
    )
    filter_location: EnumProperty(
        items=enum_filter_location,
        name="Filter by Location",
        description="Further filter talks by location",
        default='ALL',
    )

    def draw_item(self, context, layout, data, talk, icon, active_data, active_propname):
        # draw_item must handle the three layout types... Usually 'DEFAULT' and 'COMPACT' can share the same code.
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            split = layout.split(factor=0.6, align=True)
            row = split.split(factor=0.4, align=True)
            row.label(text=f"{talk.uid} {time.strftime('%H:%M %a', time.localtime(talk.timestamp_start))}",
                        translate=False, icon_value=icon)
            row.label(text=f"| {talk.title}", translate=False, icon_value=icon)
            ssub = split.split(factor=0.4, align=True)
            ssub.label(text=f"| {talk.location}", translate=False, icon_value=icon)
            ssub.label(text=(f"| !!!{talk.stage}!!!" if talk.is_stage_error else
                             f"| +{talk.stage}+" if talk.is_active else
                             f"| -{talk.stage}-"),
                        translate=False, icon_value=icon)
        elif self.layout_type == 'GRID':
            layout.alignment = 'CENTER'
            layout.label(text="%s" % talk.uid, icon_value=icon)

    def draw_filter(self, context, layout):
        row = layout.row()
        row.prop(self, "filter_category", expand=True)
        row = layout.row()
        row.prop(self, "filter_location", expand=True)

    def filter_items(self, context, data, propname):
        # This function gets the collection property (as the usual tuple (data, propname)), and must return two lists:
        # * The first one is for filtering, it must contain 32bit integers were `self.bitflag_filter_item` marks the
        #   matching item as filtered (i.e. to be shown), and 31 other bits are free for custom needs.
        # * The second one is for reordering, it must return a list containing the new indices of the items (which
        #   gives us a mapping org_idx -> new_idx).
        # * Returning an empty list means "no change".

        def is_valid_talk(self, talk, curr_time):
            fcategory = self.filter_category
            flocation = self.filter_location
            def filter_location(talk, flocation):
                return flocation in {self.FILTER_LOCATION_ALL, talk.location}
            # Never hide talks with some error status.
            if talk.is_stage_error:
                return True
            match fcategory:
                case self.FILTER_CATEGORY_ALL:
                    return filter_location(talk, flocation)
                case self.FILTER_CATEGORY_CURRENT:
                    if talk.is_active:
                        return filter_location(talk, flocation)
                    # Current time within talk range, +/- some margin (in seconds) extra.
                    if ((curr_time - 7200.0 < talk.timestamp_start < curr_time + 1800.0) or
                        (curr_time - 3600.0 < talk.timestamp_end < curr_time + 3600.0)):
                        return filter_location(talk, flocation)
                    return False
                case self.FILTER_CATEGORY_ACTIVE:
                    return talk.is_active and filter_location(talk, flocation)
                case self.FILTER_CATEGORY_TODAY:
                    if talk.timestamp_start // (3600.0 * 24.0) == curr_time // (3600.0 * 24.0):
                        return filter_location(talk, flocation)
                case self.FILTER_CATEGORY_ERROR:
                    return False

        curr_time = time.time()
        # ~ curr_time = time.mktime(time.strptime("2023/10/26 12:15:23", "%Y/%m/%d %H:%M:%S"))
        flt_flags = [self.bitflag_filter_item if is_valid_talk(self, talk, curr_time) else 0
                     for talk in getattr(data, propname)]
        # No changing of order, keep everything sorted by start time.
        flt_neworder = []

        return flt_flags, flt_neworder

    
class BCONVIDEO_PT_talks(BConVideoPanel, bpy.types.Panel):
    bl_label = "Talks"

    def draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False  # No animation.

        data = context.scene.bcon_video_data

        layout.template_list("BCONVIDEO_UL_talks", "", data, "talks", data, "talk_active_index")


class BCONVIDEO_PT_talk_process(BConVideoPanel, bpy.types.Panel):
    bl_label = "Active Talk"

    @staticmethod
    def draw_stage_upcoming(layout, data, talk, talks_manager):
        pass

    @staticmethod
    def draw_stage_acquiring(layout, data, talk, talks_manager):
        layout.operator("sequencer.bcon_video_acquire_files_select", text="Acquire Files")
        for rvid in talk.raw_videos:
            split = layout.split(factor=0.8, align=True)
            split.alert = not rvid.is_valid
            split.label(text=f"{rvid.filename}: "
                             f"({rvid.size/1024:.2f} MB, {time.strftime('%a %H:%M', time.localtime(rvid.mtime))})")
            split.operator("sequencer.bcon_video_talk_file_remove").filename = rvid.filename

    @staticmethod
    def draw_stage_processing(layout, data, talk, talks_manager):
        row = layout.row()
        row.prop(talk, "thumbnail_frame", text=bpy.utils.smpte_from_frame(talk.thumbnail_frame))
        row.operator("sequencer.bcon_video_frame_go", text="Go").frame = talk.thumbnail_frame
        row.operator("sequencer.bcon_video_processing_thumbnail_frame_set",
                     text="Clear" if talk.thumbnail_frame > 0 else "Set")
        for rvid in talk.raw_videos:
            row = layout.row()
            row.label(text=rvid.filename)
            row.operator("sequencer.bcon_video_talk_file_remove").filename = rvid.filename
            for prop_name, prop_label, op_code in (("cut_in", "In", 'CUT_IN'), ("cut_out", "Out", 'CUT_OUT')):
                isset_prop_name = "is_" + prop_name + "_set"
                isset_prop_label = prop_label + " Set"
                row = layout.row()
                sub = row.row()
                sub.enabled = not rvid.is_final_set
                sub.label(text=prop_label)
                sub.prop(rvid, prop_name, text=bpy.utils.smpte_from_frame(getattr(rvid, prop_name)))
                op = row.operator("sequencer.bcon_video_processing_cutframe_set",
                                  text="Clear" if getattr(rvid, isset_prop_name) else "Set")
                op.raw_video_uid = rvid.filename
                op.prop_set = op_code
            layout.separator()

    @staticmethod
    def draw_stage_exporting(layout, data, talk, talks_manager):
        if talk.is_export_stage_validation_required:
            layout.operator("sequencer.bcon_video_exporting_final_validate")

    @staticmethod
    def draw_stage_uploading(layout, data, talk, talks_manager):
        if talk.is_upload_stage_validation_required:
            layout.operator("sequencer.bcon_video_uploading_final_validate")

    @staticmethod
    def draw_stage_done(layout, data, talk, talks_manager):
        pass

    draw_stages = {
        stages.STAGE_UPCOMING: draw_stage_upcoming,
        stages.STAGE_ACQUIRING: draw_stage_acquiring,
        stages.STAGE_PROCESSING: draw_stage_processing,
        stages.STAGE_EXPORTING: draw_stage_exporting,
        stages.STAGE_UPLOADING: draw_stage_uploading,
        stages.STAGE_DONE: draw_stage_done,
    }

    def draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False  # No animation.

        data = context.scene.bcon_video_data
        talk_index = data.talk_active_index
        if talk_index < 0 or talk_index >= len(data.talks):
            return
        talk = data.talks[talk_index]
        talks_manager = stages.TalksManager()
        is_talk_active = talks_manager.talk_is_active(talk)

        layout.alert = talk.is_stage_error

        if IS_DEBUG:
            row = layout.row()
            row.prop(talk, "skip_stage_export")
            row.prop(talk, "skip_stage_upload")

        if is_talk_active:
            row = layout.row()
            row.operator("sequencer.bcon_video_stage_prev", text="Prev")
            stage = "%s %s" % (talks_manager.talk_status(talk), talk.stage)
            row.label(text="%s - Talk %s" % (stage, talk.uid))
            row.operator("sequencer.bcon_video_stage_next", text="Next")
        elif talk.stage == stages.STAGE_UPCOMING:
            layout.operator("sequencer.bcon_video_stage_next", text="Start Talk %s" % talk.uid)
        elif talk.stage == stages.STAGE_DONE:
            layout.operator("sequencer.bcon_video_stage_next", text="Restart Finished Talk %s" % talk.uid)
        else:
            layout.operator("sequencer.bcon_video_stage_next", text="Continue Processing Talk %s (%s)" % (talk.uid, talk.stage))

        if is_talk_active:
            layout.label(text=talk.status_message)
            if talk.stage in self.draw_stages:
                self.draw_stages[talk.stage](layout, data, talk, talks_manager)


class BCONVIDEO_PT_talk_meta_data(BConVideoPanel, bpy.types.Panel):
    bl_label = "Active Talk Meta Data"

    def draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False  # No animation.

        data = context.scene.bcon_video_data
        talk_index = data.talk_active_index
        if talk_index < 0 or talk_index >= len(data.talks):
            return
        talk = data.talks[talk_index]
        is_talk_active = talk.is_active

        layout.alert = talk.is_stage_error
        row = layout.row()
        row.label(text=f"From: {time.strftime('%a-%H:%M', time.localtime(talk.timestamp_start))}")
        row.label(text=f"To: {time.strftime('%a-%H:%M', time.localtime(talk.timestamp_end))}")
        for idx in range(10):
            layout.prop(talk, f"description_line{idx}", text="")


register_classes = (
    BCONVIDEO_UL_talks,

    BCONVIDEO_MT_operations,

    BCONVIDEO_PT_paths,
    BCONVIDEO_PT_talks,
    BCONVIDEO_PT_talk_process,
    BCONVIDEO_PT_talk_meta_data,
)


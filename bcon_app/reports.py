# SPDX-License-Identifier: GPL-2.0-or-later

import time


REPORT_INFO = "INFO"
REPORT_WARNING = "WARNING"
REPORT_ERROR = "ERROR"
REPORT_TYPES = {REPORT_INFO, REPORT_WARNING, REPORT_ERROR}

class Report:
    def __init__(self, report_type, report_message, *args, **kwargs):
        if report_type not in REPORT_TYPES:
            report_type = REPORT_ERROR
        self.type = report_type
        self.message = report_message
        self.data = args
        self.kw_data = kwargs
        self.timestamp = time.time()

    def __repr__(self):
        return f"{self.type}: {self.message}"


class ReportsHandler:
    def __init__(self):
        self.reports = []
        # Quick access to types of reports currently stored in this handler.
        self.reports_types = set()

    def add_info(self, message, *args, **kwargs):
        self.reports.append(Report(REPORT_INFO, message, args, kwargs))
        self.reports_types.add(REPORT_INFO)

    def add_warning(self, message, *args, **kwargs):
        self.reports.append(Report(REPORT_WARNING, message, args, kwargs))
        self.reports_types.add(REPORT_WARNING)

    def add_error(self, message, *args, **kwargs):
        self.reports.append(Report(REPORT_ERROR, message, args, kwargs))
        self.reports_types.add(REPORT_ERROR)

    def clear(self, types=REPORT_TYPES):
        self.reports = [rprt for rprt in self.reports if rprt.type not in types]
        self.reports_types -= types

    @property
    def has_info(self):
        return REPORT_INFO in self.reports_types

    @property
    def has_warning(self):
        return REPORT_WARNING in self.reports_types

    @property
    def has_error(self):
        return REPORT_ERROR in self.reports_types

    def __repr__(self):
        return "REPORTS:\n" + "\n".join(f"\t{report}" for report in self.reports) + "\n"


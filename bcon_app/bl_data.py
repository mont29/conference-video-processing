# SPDX-License-Identifier: GPL-2.0-or-later

from . import (
    settings,
    stages,
    bl_ops,
)
if "bpy" in locals():
    import importlib
    if "settings" in locals():
        importlib.reload(settings)
    if "stages" in locals():
        importlib.reload(stages)
    if "bl_ops" in locals():
        importlib.reload(bl_ops)


import bpy
from bpy.props import (
        StringProperty,
        BoolProperty,
        FloatProperty,
        IntProperty,
        EnumProperty,
        CollectionProperty,
        PointerProperty,
        )


import json
import os
import pprint
import shutil
import time


VALID_TALK_CATEGORIES = settings.VALID_TALK_CATEGORIES
INVALID_TALK_TITLE_MATCH = settings.INVALID_TALK_TITLE_MATCH
VIDEO_DESCRIPTION = settings.VIDEO_DESCRIPTION
VIDEO_LOCATION = settings.VIDEO_LOCATION


# 'Event Loop', here because update callbacks of some properties need to kick it.

class BConVP_Timer:
    """Singleton callable class, to make it clear/easier to store status info (for single-time delayed calls e.g.)."""
    def __new__(cls):
        if getattr(cls, 'instance', None) is None:
            cls.instance = super(BConVP_Timer, cls).__new__(cls)
            cls.instance.set_bcon_tab_active = True
        return cls.instance

    def __call__(self):
        """
        Timer callback used to handle updates of active tasks, and backup of current
        status in the persistent json file.
        """
        # ~ print("Start timer")

        # Try to set bcon app tab active.
        # Note that it may fail if called too early (before any drawing call happened, no category are registered/known).
        # This is why the the flag is only cleared if tab is succefully set.
        if self.set_bcon_tab_active:
            # ~ print("Setting Tab Active")
            screen = bpy.context.screen
            for area in screen.areas:
                if area.type != 'SEQUENCE_EDITOR':
                    continue
                for region in area.regions:
                    if region.type != 'UI':
                        continue
                    try:
                        region.active_panel_category = 'BCon Video'
                    except AttributeError:
                        pass
                    if region.active_panel_category == 'BCon Video':
                        self.set_bcon_tab_active = False
                    region.tag_redraw()


        bcon_data = bpy.context.scene.bcon_video_data
        if bcon_data.do_lock_timer:
            # ~ print("Timer Locked")
            return None
        if not bcon_data.paths.has_valid_paths:
            # ~ print("Stop timer - Invalid Paths")
            return None
        if not bcon_data.talks:
            bcon_data.update_from_persistent_data()

        talks_manager = stages.TalksManager()
        for bl_talk in bcon_data.talks:
            if talks_manager.talk_is_active(bl_talk):
                status = talks_manager.talk_update(bpy.context, bl_talk)
                # ~ print(status)

        # Brute force, but otherwise it's hard to get proper UI update...
        for area in bpy.context.screen.areas:
            if area.type != 'SEQUENCE_EDITOR':
                continue
            area.tag_redraw()

        bcon_data.update_to_persistent_data()
        if bcon_data.active_talks_num > 0 or self.set_bcon_tab_active:
            # ~ print("Continue timer")
            return 0.2
        # ~ print("Stop timer")
        return None


def update_timer_kick(self_, context_):
    """
    Update callback to kick update timer above.
    """
    bcon_vp_timer = BConVP_Timer()
    if not bpy.app.timers.is_registered(bcon_vp_timer):
        bpy.app.timers.register(bcon_vp_timer, persistent=True)


@bpy.app.handlers.persistent
def load_post_handler(file_):
    """
    Update callback after loading a file, will ensure initial updates of bcon data is done.
    """
    bcon_data = bpy.context.scene.bcon_video_data
    bcon_data.update_from_persistent_data()
    bcon_data.update_to_persistent_data()
    bcon_data.update_from_reference_data()
    bcon_data.reset_talks_manager(bpy.context)

    bcon_vp_timer = BConVP_Timer()
    bcon_vp_timer.set_bcon_tab_active


# Blender-level data definition and handling.

def bl_collection_prop_cleanup_by_keys(bl_data_prop, del_keys=None, keep_keys=None):
    """
    Utils to remove some items from RNA collections based on (string) keys.
    """
    if del_keys is not None:
        if keep_keys is not None:
            del_keys -= keep_keys
    elif keep_keys is not None:
        del_keys = set(bl_data_prop.keys()) - keep_keys
    else:
        return
    # Note that only indices can be used to remove items from a py-defined CollectionProperty.
    del_indices = []
    for idx, item_k in enumerate(bl_data_prop.keys()):
        if item_k in del_keys:
            del_indices.append(idx)
    for idx in del_indices[::-1]:
        bl_data_prop.remove(idx)


class BLDictGenerator:
    """
    Helper class to add generic import/export of data to a python dictionary (for serialization to simple formats like JSon).
    Supports sub-data from Pointer or Collection properties too.
    """
    # `bl_rna` properties to ignore, allways only `add()` to this set.
    # `name` is a propoerty hard-coded in PropertyGroup, used for collection's subscript access by string key.
    dict_ignore_properties = {"rna_type", "name"}

    def to_dict(self):
        data = {}
        if not hasattr(self, "bl_rna"):
            return data
        for bl_prop in self.bl_rna.properties:
            key = bl_prop.identifier
            if key in self.dict_ignore_properties:
                continue
            if isinstance(bl_prop, bpy.types.CollectionProperty):
                if not bl_prop.fixed_type:
                    continue
                item_k_prop = bl_prop.fixed_type.name_property.identifier if bl_prop.fixed_type.name_property else None
                if item_k_prop:
                    data[key] = {}
                    for item in getattr(self, key):
                        if item is None or not hasattr(item, "to_dict"):
                            continue
                        item_k = getattr(item, item_k_prop)
                        data[key][item_k] = item.to_dict()
                else:
                    data[key] = []
                    for item in getattr(self, key):
                        if item is None or not hasattr(item, "to_dict"):
                            continue
                        data[key].append(item.to_dict())
            elif isinstance(bl_prop, bpy.types.PointerProperty):
                sub_data = getattr(self, key)
                if sub_data is None or not hasattr(sub_data, "to_dict"):
                    continue
                data[key] = sub_data.to_dict()
            elif bl_prop.is_readonly:
                # Do not save simple property values that cannot be restored.
                continue
            else:
                data[key] = getattr(self, key)
        return data

    def from_dict(self, dict_data):
        if not hasattr(self, "bl_rna"):
            return
        for key, value in dict_data.items():
            if key in self.dict_ignore_properties:
                continue
            if key not in self.bl_rna.properties:
                continue
            bl_prop = self.bl_rna.properties.get(key)
            if isinstance(bl_prop, bpy.types.CollectionProperty):
                if not bl_prop.fixed_type:
                    continue
                sub_data = getattr(self, key)
                if sub_data is None:
                    continue
                if isinstance(value, dict):
                    if not bl_prop.fixed_type.name_property:
                        continue
                    item_k_prop = bl_prop.fixed_type.name_property.identifier
                    # Clean-up data that does not exist in given dict.
                    bl_collection_prop_cleanup_by_keys(sub_data, keep_keys=value.keys())
                    # Add or update items from given dict.
                    for item_k, item_v in value.items():
                        if item_k not in sub_data:
                            item = sub_data.add()
                            setattr(item, item_k_prop, item_k)
                        else:
                            item = sub_data[item_k]
                        item.from_dict(item_v)
                elif hasattr(value, "__iter__"):
                    len_value = len(value)
                    len_sub_data = len(sub_data)
                    if len_value < len_sub_data:
                        for i in range(len_sub_data - 1, len_value - 1, -1):
                            sub_data.remove(i)
                    elif len_value < len_sub_data:
                        for i in range(len_sub_data, len_value):
                            sub_data.add()
                    assert(len(sub_data) == len_value)
                    for item, item_v in zip(sub_data, value):
                        item.from_dict(item_v)
            elif isinstance(bl_prop, bpy.types.PointerProperty):
                sub_data = getattr(self, key)
                if sub_data is None or not hasattr(sub_data, "from_dict") or not isinstance(value, dict):
                    continue
                sub_data.from_dict(value)
            else:
                setattr(self, key, value)


def use_propertygroup_name(deferred_prop):
    """
    Utils to allow using another string property identifier than 'name'
    as key property for items of an RNA collection.
    """
    def setter(self, value):
        self.name = value
    deferred_prop.keywords["get"] = lambda self: self.name
    deferred_prop.keywords["set"] = setter
    return deferred_prop


class BConVideoFilePath(bpy.types.PropertyGroup, BLDictGenerator):
    """
    Dummy property group for collections of filepaths.
    """
    path: StringProperty(
        name="File Path",
        description="Path to the file",
        default="",
        subtype='FILE_PATH',
    )


class BConVideoRawFileData(bpy.types.PropertyGroup, BLDictGenerator):
    """
    Simplified representation of a VSE strip, and collection of paths for that video file.
    """
    filename: use_propertygroup_name(StringProperty(
        name="Filename",
        description="The unique file name of this raw video, used as identifier, "
                    "and in all destination paths of copy tasks (Note: cropped to 32 chars)",
        maxlen=32,
        default="",
    ))

    # File paths.
    path_src: StringProperty(
        name="Source Path",
        description="Source path of a raw video file to process (typically on an external storage)",
        default="",
        subtype='FILE_PATH',
    )
    path_work: StringProperty(
        name="Work Path",
        description="Work path of a raw video file to process (typically on a local storage)",
        default="",
        subtype='FILE_PATH',
    )
    paths_backup: CollectionProperty(
        type=BConVideoFilePath,
        name="Backup Paths",
        description="Where to copy backups of the raw source video file (typically on a backup server)",
    )

    # Meta-data
    is_valid: BoolProperty(
        name="Is Valid",
        description="Whether this video file is valid for the process",
        default=False,
    )
    size: IntProperty(
        name="Size",
        description="File size on disk, in kilobytes",
        default=0,
    )
    mtime: FloatProperty(
        name="Last Modified",
        description="Timestamp of last modification of this file",
        default=0.0,
    )

    # VSE (strip) info.
    index: IntProperty(
        name="Strip Index",
        description="Index (order) of this file in the talk, only used in case there are more than one video files",
        default=0,
    )
    # Values in raw video, defined by user.
    cut_in: FloatProperty(
        name="Cut In Frame",
        description="First frame of the strip to (fully) show (does not include the extra intro fade-in)",
        subtype='TIME',
        unit='TIME',
    )
    is_cut_in_set: BoolProperty(
        name="Cut In Defined",
        description="Cut-in timestamp of the video has been defined",
    )
    cut_out: FloatProperty(
        name="Cut Out Frame",
        description="Last frame of the strip to (fully) show (does not include the extra outro fade-out)",
        subtype='TIME',
        unit='TIME',
    )
    is_cut_out_set: BoolProperty(
        name="Cut Out Defined",
        description="Cut-out timestamp of the video has been defined",
    )
    # Final settings, typically automatically set by the tool.
    is_final_set: BoolProperty(
        name="Is Final",
        description="These timings are defined for the final export, i.e. no automatic placement should happen anymore",
    )
    final_start: FloatProperty(
        name="Start Frame",
        description="First frame of the strip in the sequencer, not taking into account the offset from `cut_in`",
        subtype='TIME',
        unit='TIME',
    )
    final_cut_in: FloatProperty(
        name="Cut In Frame",
        description="First frame of the strip to show (does include the extra intro fade-in)",
        subtype='TIME',
        unit='TIME',
    )
    final_cut_out: FloatProperty(
        name="Cut Out Frame",
        description="Last frame of the strip to show (does include the extra outro fade-out)",
        subtype='TIME',
        unit='TIME',
    )


class BConVideoDataTalk(bpy.types.PropertyGroup, BLDictGenerator):
    """
    The Blender representation of a Talk.
    """
    dict_ignore_properties = BLDictGenerator.dict_ignore_properties | {f"description_line{idx}" for idx in range(10)} | {"status_message"}

    # `name` if pre-defined property name to allow subscript by text in CollectionProperty of PropertyGroup.
    uid: use_propertygroup_name(StringProperty(
        name="UID",
        description="UID of a talk",
    ))

    title: StringProperty(
        name="Title",
        description="Title of the talk",
    )

    timestamp_start: FloatProperty(
        name="Start Date Time",
        description="Date and time of the scheduled start of the talk",
        subtype='TIME_ABSOLUTE',
        unit='TIME_ABSOLUTE',
    )
    timestamp_end: FloatProperty(
        name="End Date Time",
        description="Date and time of the scheduled end of the talk",
        subtype='TIME_ABSOLUTE',
        unit='TIME_ABSOLUTE',
    )

    speakers: StringProperty(
        name="Speakers",
        description="Comma-separated lists of the spealkers' names",
    )
    location: StringProperty(
        name="Location",
        description="Where is the talk happening",
    )
    location_full: StringProperty(
        name="Full Location",
        description="Full version of where is the talk happening (used for Youtube video description)",
    )
    description: StringProperty(
        name="Description",
        description="Short description of the talk (used for Youtube video)",
    )

    # Cheap work-around lack of multi-line text editing widget...
    def description_line_getter_gen(line_idx):
        def description_line_getter(self):
            lines = self.description.split("\n")
            return lines[line_idx] if line_idx < len(lines) else ""
        return description_line_getter
    def description_line_setter_gen(line_idx):
        def description_line_setter(self, value):
            lines = self.description.split("\n")
            if len(lines) <= line_idx:
                lines += ["" for i in range(len(lines), line_idx)]
            self.description = "\n".join(lines[:line_idx] + [value] + lines[line_idx + 1:])
        return description_line_setter
    description_line0: StringProperty(
        name="Description Line 1",
        description="Line 1 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(0),
        set=description_line_setter_gen(0),
    )
    description_line1: StringProperty(
        name="Description Line 2",
        description="Line 2 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(1),
        set=description_line_setter_gen(1),
    )
    description_line2: StringProperty(
        name="Description Line 3",
        description="Line 3 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(2),
        set=description_line_setter_gen(2),
    )
    description_line3: StringProperty(
        name="Description Line 4",
        description="Line 4 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(3),
        set=description_line_setter_gen(3),
    )
    description_line4: StringProperty(
        name="Description Line 5",
        description="Line 5 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(4),
        set=description_line_setter_gen(4),
    )
    description_line5: StringProperty(
        name="Description Line 6",
        description="Line 6 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(5),
        set=description_line_setter_gen(5),
    )
    description_line6: StringProperty(
        name="Description Line 7",
        description="Line 7 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(6),
        set=description_line_setter_gen(6),
    )
    description_line7: StringProperty(
        name="Description Line 8",
        description="Line 8 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(7),
        set=description_line_setter_gen(7),
    )
    description_line8: StringProperty(
        name="Description Line 9",
        description="Line 9 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(8),
        set=description_line_setter_gen(8),
    )
    description_line9: StringProperty(
        name="Description Line 10",
        description="Line 10 of the description of the talk (used for Youtube video)",
        get=description_line_getter_gen(9),
        set=description_line_setter_gen(9),
    )

    enum_stages = (
        (stages.STAGE_UPCOMING, "Upcoming", "", 'ICON_NONE', 0),
        (stages.STAGE_ACQUIRING, "Acquiring", "", 'ICON_NONE', 10),
        (stages.STAGE_PROCESSING, "Processing", "", 'ICON_NONE', 20),
        (stages.STAGE_EXPORTING, "Exporting", "", 'ICON_NONE', 30),
        (stages.STAGE_UPLOADING, "Uploading", "", 'ICON_NONE', 40),
        (stages.STAGE_DONE, "Done", "", 'ICON_NONE', 60),
    )
    stage: EnumProperty(
        items=enum_stages,
        name="Stage",
        description="Current stage of this talk's processing",
        default='UPCOMING',
    )
    is_stage_pending: BoolProperty(
        name="Pending Stage",
        description="The current stage is pending finishing some background tasks from previous stage(s) to become active",
        default=False,
    )
    is_stage_error: BoolProperty(
        name="Error",
        description="The current stage has errors preventing further processing",
        default=False,
    )

    def is_active_get(self):
        talks_manager = stages.TalksManager()
        return talks_manager.talk_is_active(self)
    is_active: BoolProperty(
        name="Is Active",
        description="Whether this talk is currently active, being processed",
        get=is_active_get,
    )

    def status_message_get(self):
        talks_manager = stages.TalksManager()
        return talks_manager.talk_status_message(self)
    status_message: StringProperty(
        name="Status Message",
        description="Status message from the active stage/tasks",
        get=status_message_get,
    )

    work_dir: StringProperty(
        name="Base Work Path",
        description="Where runtime data is stored during processing of this talk",
        default="",
        subtype='DIR_PATH',
    )
    raw_videos: CollectionProperty(
        type=BConVideoRawFileData,
        name="Source Videos",
        description="Source paths of the video files to process for this talk, with basic editing info",
    )
    raw_videos_counter: IntProperty(
        name="Source Videos Counter",
        description="Strictly always-increasing counter for raw videos, "
                    "used to ensure all work files names generated have an increasing index part",
        default=0,
    )

    thumbnail_frame: IntProperty(
        name="Preview Thumbnail Frame",
        description="Which frame to render as thumbnail preview for Youtube upload",
        default=0,
    )
    thumbnail_path: StringProperty(
        name="Thumbnail Export Path",
        description="Destination path of the final thumbnail image",
        default="",
        subtype='FILE_PATH',
    )
    title_card_render_frame: IntProperty(
        name="TitleCard Render Frame",
        description="Which frame to render for the title card",
        default=0,
    )
    export_path: StringProperty(
        name="Export Path",
        description="Destination path of the final export video",
        default="",
        subtype='FILE_PATH',
    )
    export_backup_path: StringProperty(
        name="Export Backup Path",
        description="Where to copy a backup of the final export",
        default="",
        subtype='DIR_PATH',
    )
    is_export_stage_validation_required: BoolProperty(
        name="Is Export Validation Required",
        description="Whether user should validate the final export video",
        default=False,
    )
    is_export_stage_validated: BoolProperty(
        name="Is Export Valid",
        description="Whether user has validated the final export result",
        default=False,
    )
    skip_stage_export: BoolProperty(
        name="Skip Export Stage",
        description="Unconditionnaly skip export stage, mostly useful for development and debugging",
    )

    youtube_url: StringProperty(
        name="Youtube URL",
        description="URL of the uploaded video on Youtube",
        default="",
    )
    youtube_edit_url: StringProperty(
        name="Youtube Edit URL",
        description="URL to edit the uploaded video on Youtube",
        default="",
    )
    is_upload_stage_validation_required: BoolProperty(
        name="Is Upload Validation Required",
        description="Whether user should validate the final uploaded video",
        default=False,
    )
    is_upload_stage_validated: BoolProperty(
        name="Is Upload Valid",
        description="Whether user has validated the final uploaded result",
        default=False,
    )
    skip_stage_upload: BoolProperty(
        name="Skip Upload Stage",
        description="Unconditionnaly skip upload stage, mostly useful for development and debugging",
    )

    def manager_talk_status_get(self):
        talks_manager = stages.TalksManager()
        if talks_manager.talk_is_active(self):
            return True, talks_manager.talk_stage(self), talks_manager.talk_status(self)
        else:
            return False, None, None

    def manager_talk_update(self, context):
        talks_manager = stages.TalksManager()
        if talks_manager.talk_is_active(self):
            return True, talks_manager.talk_update(context, self)
        else:
            return False, None


class BConVideoDataDirectories(bpy.types.PropertyGroup, BLDictGenerator):
    """
    Set of paths (more like general settings for the addons?).
    """
    def dirspace_getter_gen(dirpath_propname):
        def dirspace_getter(self):
            dirpath = getattr(self, dirpath_propname)
            if not dirpath or not os.path.exists(dirpath):
                return 0.0
            total, used, free = shutil.disk_usage(dirpath)
            return used / total * 100.0
        return dirspace_getter

    reference_path: StringProperty(
        name="Reference DB File",
        description="JSON file used as (read-only) initial reference for the BCon talks schedule",
        default=os.path.join(settings.WORK_DATA_ROOT, "presentations.json"),
        subtype='FILE_PATH',
        update=update_timer_kick,
    )
    persistent_path: StringProperty(
        name="Persistent DB File",
        description="JSON file used to store reference data for the BCon talks processing, kept in sync with the reference one",
        default=os.path.join(settings.WORK_DATA_ROOT, "bcon_video_data.json"),
        subtype='FILE_PATH',
        update=update_timer_kick,
    )

    print(settings.WORK_DATA_ROOT)
    raw_videos_dir: StringProperty(
        name="Raw Videos Directory",
        description="Where to put acquired raw video files (the root working directory)",
        default=settings.WORK_DATA_ROOT,
        subtype='DIR_PATH',
        update=update_timer_kick,
    )
    raw_videos_dir_space: FloatProperty(
        name="Available Space",
        description="Available space in the directory storing raw video files, as a percentage",
        subtype='PERCENTAGE',
        get=dirspace_getter_gen('raw_videos_dir'),
    )

    backup_dir: StringProperty(
        name="Backup Directory",
        description="Root path to the backup directory (for final videos storage)",
        default="",
        subtype='DIR_PATH',
    )
    backup_dir_space: FloatProperty(
        name="Available Space",
        description="Available space in the backup directory for videos, as a percentage",
        subtype='PERCENTAGE',
        get=dirspace_getter_gen('backup_dir_space'),
    )

    show_paths: BoolProperty(
        name="Show Paths",
        description="Wether to show paths in the UI, or not",
        default=False,
    )

    def has_valid_paths_get(self):
        return (os.path.isfile(self.reference_path) and
                os.path.isdir(self.raw_videos_dir) and
                (os.path.isfile(self.persistent_path) or not self.persistent_path))
    has_valid_paths: BoolProperty(get=has_valid_paths_get)


class BConVideoData(bpy.types.PropertyGroup, BLDictGenerator):
    """
    The general data container for the BCon Video Processing tool.
    """
    paths: PointerProperty(type=BConVideoDataDirectories)

    def active_talk_update(self, context):
        if self.talks:
            if self.talk_active_index < 0:
                self.talk_active_index = 0
            if self.talk_active_index >= len(self.talks):
                self.talk_active_index = len(self.talks) - 1
            active_talk = self.talks[self.talk_active_index]
            vse = context.scene.sequence_editor
            if vse is None:
                return
            vse.display_stack(None)
            for chan in vse.channels:
                chan.mute = True
            if not active_talk.is_active:
                return
            if active_talk.uid not in vse.channels or active_talk.uid not in vse.sequences:
                return
            talk_channel = vse.channels[active_talk.uid]
            talk_channel.mute = False
            talk_meta_strip = vse.sequences[active_talk.uid]
            assert talk_meta_strip.type == 'META'
            vse.display_stack(talk_meta_strip)

    talks: CollectionProperty(type=BConVideoDataTalk)
    talk_active_index: IntProperty(default=-1, update=active_talk_update)

    do_lock_timer: BoolProperty(
        name="Lock Timer",
        description="Lock the update timer to prevent it from running (runtime data)",
    )
    dict_ignore_properties = BLDictGenerator.dict_ignore_properties | {"do_lock_timer"}

    def active_talks_num_get(self):
        return len([talk for talk in self.talks if talk.is_active])
    active_talks_num: IntProperty(get=active_talks_num_get)

    def update_from_reference_data(self):
        if not self.paths.raw_videos_dir or not self.paths.reference_path:
            return
        valid_data = []
        if os.path.exists(self.paths.persistent_path):
            with open(self.paths.reference_path) as js_file:
                js_data = json.load(js_file)
            valid_data = [(key, data, time.mktime(time.strptime(data["day"] + data["start_time"], "%Y-%m-%d%H:%M")), data["location"])
                          for key, data in js_data.items()
                          if data.get("category") in VALID_TALK_CATEGORIES and data.get("speakers") and
                             not any(it in data.get("title") for it in INVALID_TALK_TITLE_MATCH)]
        valid_data.sort(key=lambda d: (d[2], d[3]))
        valid_keys = {d[0] for d in valid_data}
        bl_collection_prop_cleanup_by_keys(self.talks, keep_keys=valid_keys)
        for key, data, timestamp, location in valid_data:
            talk = self.talks.get(key, None)
            if talk is None:
                talk = self.talks.add()
                talk.uid = key
            talk.work_dir = os.path.join(self.paths.raw_videos_dir, str(talk.uid))
            talk.timestamp_start = timestamp
            talk.timestamp_end = timestamp + float(data["duration_minutes"]) * 60.0
            talk.title = data["title"]
            talk.speakers = data["speakers"]
            talk.location = location

            # Additional richer meta-data generated from raw reference data.
            # Remove any trailing ', ' in case there is no location set.
            talk.location_full = VIDEO_LOCATION.format(location=talk.location).strip().rstrip(',')
            description = VIDEO_DESCRIPTION.format(
                title=talk.title,
                speakers=talk.speakers,
                day=data["day"],
                start_time=data["start_time"],
                location=talk.location_full,
            )
            if talk.description != description:
                if talk.description:
                    talk.description = (
                        "!!!!START Automatically Generated!!!!\n" + description +
                        "!!!!END Automatically Generated!!!!\n" + talk.description
                    )
                else:
                    talk.description = description

    def update_from_persistent_data(self):
        self.do_lock_timer = True
        if self.paths.persistent_path and os.path.exists(self.paths.persistent_path):
            with open(self.paths.persistent_path) as js_file:
                js_data = json.load(js_file)
            self.from_dict(js_data)
        if bpy.context.scene.sequence_editor is None:
            bpy.context.scene.sequence_editor_create()
        self.active_talk_update(bpy.context)
        self.update_to_persistent_data()
        self.do_lock_timer = False

    def update_to_persistent_data(self):
        if not self.paths.raw_videos_dir:
            return
        if not self.paths.persistent_path:
            self.paths.persistent_path = os.path.join(self.paths.raw_videos_dir, "bcon_video_data.json")
        js_data = self.to_dict()
        tmp_path = self.paths.persistent_path + ".tmp"
        with open(tmp_path, "w") as js_file:
            json.dump(js_data, js_file, indent=4)
        os.replace(tmp_path, self.paths.persistent_path)

    def reset_talks_manager(self, context):
        talks_manager = stages.TalksManager()
        talks_manager.clear()

        for talk in self.talks:
            if talk.stage not in stages.STAGES:
                talk.stage = stages.STAGE_UPCOMING
            if talk.stage in {stages.STAGE_UPCOMING, stages.STAGE_DONE}:
                continue
            if not talks_manager.talk_is_active(talk):
                talks_manager.talk_init(context, talk)


register_classes = (
    BConVideoFilePath,
    BConVideoRawFileData,
    BConVideoDataDirectories,
    BConVideoDataTalk,
    BConVideoData,
)


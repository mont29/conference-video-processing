# SPDX-License-Identifier: GPL-2.0-or-later

if "settings" in locals():
    import importlib
    importlib.reload(settings)
else:
    from . import settings
if "reports" in locals():
    import importlib
    importlib.reload(reports)
else:
    from . import reports


import concurrent.futures
import json
import os
import stat
import shutil
import subprocess
from pprint import pprint


IS_DEBUG = settings.IS_DEBUG
FPS = settings.FPS
AUDIO_PRIMING_SAMPLE_CORRECTION = settings.AUDIO_PRIMING_SAMPLE_CORRECTION
TITLECARD_LENGTH = settings.TITLECARD_LENGTH
TITLECARD_XFADE = settings.TITLECARD_XFADE


STAGE_UPCOMING = "UPCOMING"
STAGE_ACQUIRING = "ACQUIRING"
STAGE_PROCESSING = "PROCESSING"
STAGE_EXPORTING = "EXPORTING"
STAGE_UPLOADING = "UPLOADING"
STAGE_DONE = "DONE"

STAGES = {
    STAGE_UPCOMING,
    STAGE_ACQUIRING,
    STAGE_PROCESSING,
    STAGE_EXPORTING,
    STAGE_UPLOADING,
    STAGE_DONE,
}


# Talk, stage or task is ready to start. Nothing has been done yet.
STATUS_READY = "READY"
# Stage only. The stage has been requested to become active, but not all pre-conditions are met yet.
# Typically the previous stage is in finalizing status then.
STATUS_PENDING = "PENDING"
# Talk, stage or task is active, user can interract with it (if needed).
STATUS_ACTIVE = "ACTIVE"
# Stage only. The stage is finalizing some automated processes, user can no more interract with it.
# Typically the next stage can go in pending mode then.
STATUS_FINALIZING = "FINALIZING"
# Talk, stage or task is fully done.
# NOTE: For stages, it means that all of its synchronous and blocking tasks are done,
#       some asynchronous non-blocking ones may still be active or finalizing.
# NOTE: For talks, it means that all stages and tasks, even asynchronous non-blocking ones, are done.
STATUS_DONE = "DONE"
# Some critical error happened, that could not be handled by the code
# and prevented the talk, stage or task to be successfully processed.
STATUS_ERROR = "ERROR"


class Task:
    statuses = {
        STATUS_READY: 0,
        STATUS_ACTIVE: 1,
        STATUS_DONE: 2,
        STATUS_ERROR: 100,
    }
    max_retries = 3
    is_async = False
    is_stage_blocking = False

    def __init__(self, stage):
        self.reports = reports.ReportsHandler()
        self.stage = stage
        self._status = STATUS_READY
        self.retries = 0

    def initialize(self):
        # Only time where status can be forced out of ERROR.
        self._status = STATUS_READY
        self.reports.clear()

    def start(self):
        self.status = STATUS_ACTIVE

    def retry(self):
        if self.retries < self.max_retries:
            self.cancel()
            self.retries += 1
            self.initialize()
            self.start()
            return
        self.status = STATUS_ERROR
        self.reports.add_error(f"Could not successfuly execute this is task in {self.retries} attempts", self)

    def update(self):
        pass

    def cancel(self):
        self.reports.clear()
        self._status = STATUS_READY

    def finalize(self):
        self.status = STATUS_DONE

    @property
    def status(self):
        if self.reports.has_error:
            self._status = STATUS_ERROR
        return self._status

    @status.setter
    def status(self, status):
        assert status in self.statuses
        if self._status != STATUS_ERROR:
            self._status = status


class TaskAsync(Task):
    is_async = True

    def __init__(self, stage, executor):
        super().__init__(stage)
        self.executor = executor
        self.future = None
        self.future_result = ...

    def update(self):
        if self.future:
            if not self.future.done():
                return ...
            self.finalize()
        return self.future_result

    def cancel(self):
        if self.future:
            self.future.cancel()
            self.future = None
        self.future_result = ...
        super().cancel()

    def finalize(self):
        if self.future:
            # NOTE: Blocking.
            self.future_result = self.future.result()
            self.future = None
        super().finalize()


class TaskFileCopyAsync(TaskAsync):
    is_stage_blocking = True

    def __init__(self, stage, executor, path_src, path_dst, do_replace_dst=False):
        super().__init__(stage, executor)
        self.path_src = path_src
        self.path_dst = path_dst
        self.do_replace_dst = do_replace_dst

    @staticmethod
    def async_copy(path_src, path_dst, reports):
        stat_src = os.stat(path_src)
        path_dst_exists = os.path.exists(path_dst)

        if path_dst_exists:
            stat_dst = os.stat(path_dst)
            if stat.S_ISREG(stat_dst.st_mode):
                if (stat_dst.st_size == stat_src.st_size and
                    stat_dst.st_mtime == stat_src.st_mtime):
                    # Consider the copy has already been done.
                    reports.add_info(
                        f"File `{path_src}` has already been copied to `{path_dst}`.")
                    return reports
            elif self.do_replace_dst:
                reports.add_warning(
                    f"File `{path_dst}` already present, removing as requested.")
                os.remove(path_dst)
            else:
                reports.add_error(
                    f"File `{path_src}` cannot be copied to `{path_dst}`, "
                    "destination already exists.")
                return reports

        dir_dst = os.path.dirname(path_dst)
        if not os.path.exists(dir_dst):
            os.makedirs(dir_dst)
        # In some cases, copying metadata will fail (e.g. when copying to a Samba destination).
        # Some checks based on `stat` of destination path after copy also have to be skipped then (e.g. `st_mtime` one).
        skip_dst_full_stat_check = False
        try:
            copy2_path = shutil.copy2(path_src, path_dst)
        except OSError:
            skip_dst_full_stat_check = True
            copy2_path = shutil.copyfile(path_src, path_dst)

        stat_dst = os.stat(path_dst)
        print(stat_dst)
        if stat.S_ISREG(stat_dst.st_mode):
            if (stat_dst.st_size == stat_src.st_size and
                (skip_dst_full_stat_check or stat_dst.st_mtime == stat_src.st_mtime)):
                # Successful copy.
                return reports
        reports.add_error(
            f"File `{path_src}` could not be copied to `{path_dst}`.")
        return reports

    def start(self):
        if not self.executor:
            return
        super().start()
        self.future = self.executor.submit(self.async_copy, self.path_src, self.path_dst, self.reports)


class TaskFileBackupAsync(TaskFileCopyAsync):
    is_stage_blocking = False

    def __init__(self, stage, executor, path_src, path_dst):
        super().__init__(stage, executor, path_src, path_dst)


class TaskFileSymlinkAsync(TaskAsync):
    is_stage_blocking = True

    def __init__(self, stage, executor, path_src, path_dst):
        super().__init__(stage, executor)
        self.path_src = path_src
        self.path_dst = path_dst

    @staticmethod
    def async_symlink(path_src, path_dst, reports):
        if os.path.exists(path_dst):
            os.remove(path_dst)

        dir_dst = os.path.dirname(path_dst)
        if not os.path.exists(dir_dst):
            os.makedirs(dir_dst)

        os.symlink(path_src, path_dst)

        return reports

    def start(self):
        super().start()
        if self.executor:
            self.future = self.executor.submit(self.async_symlink, self.path_src, self.path_dst, self.reports)
            return
        self.future_result = self.async_symlink(self.path_src, self.path_dst, self.reports)
        self.finalize()


class TaskFileStatAsync(TaskAsync):
    is_stage_blocking = True

    def __init__(self, stage, executor, path):
        super().__init__(stage, executor)
        self.path = path

    def start(self):
        super().start()
        if self.executor:
            self.future = self.executor.submit(os.stat, self.path)
            return
        self.future_result = os.stat(self.path)
        self.finalize()


class TaskPathDeleteAsync(TaskAsync):
    """Delete a file, or a directory with all of its content"""
    is_stage_blocking = True

    def __init__(self, stage, executor, path):
        super().__init__(stage, executor)
        self.path = path

    @staticmethod
    def async_rmtree(path, reports):
        if not os.path.exists(path):
            return reports

        if os.path.isfile(path):
            os.remove(path)
            return reports

        import shutil
        shutil.rmtree(path)

        return reports

    def start(self):
        super().start()
        if self.executor:
            self.future = self.executor.submit(self.async_rmtree, self.path, self.reports)
            return
        self.future_result = self.async_rmtree(self.path, self.reports)
        self.finalize()


class TaskProcessingCreateTitlecard(Task):
    TC_SCENE_BASE_NAME = "title_card"
    TC_COLL_BASE_NAME = "title_card_text"
    TC_OB_TXT_TITLE_BASE_NAME = "title_card_text_title"
    TC_OB_TXT_SPEAKERS_BASE_NAME = "title_card_text_speakers"

    @staticmethod
    def gen_task_name(base_name, bl_task):
        return base_name + "_" + bl_task.uid

    def __init__(self, stage, bl_talk):
        super().__init__(stage)
        self.bl_talk = bl_talk

    def start(self):
        super().start()
        import bpy
        import random

        talk_uid = self.bl_talk.uid
        tc_scene_base = self.TC_SCENE_BASE_NAME
        tc_scene_name = self.gen_task_name(tc_scene_base, self.bl_talk)
        tc_coll_base = self.TC_COLL_BASE_NAME
        tc_coll_name = self.gen_task_name(tc_coll_base, self.bl_talk)
        tc_ob_txt_title_base = self.TC_OB_TXT_TITLE_BASE_NAME
        tc_ob_txt_title_name = self.gen_task_name(tc_ob_txt_title_base, self.bl_talk)
        tc_ob_txt_speakers_base = self.TC_OB_TXT_SPEAKERS_BASE_NAME
        tc_ob_txt_speakers_name = self.gen_task_name(tc_ob_txt_speakers_base, self.bl_talk)
        if tc_scene_name not in bpy.data.scenes:
            tc_scene = bpy.data.scenes[tc_scene_base].copy()
            tc_scene.name = tc_scene_name
            
            tc_coll_orig = tc_scene.collection.children[tc_coll_base]
            tc_scene.collection.children.unlink(tc_coll_orig)
            tc_coll = tc_coll_orig.copy()
            tc_coll.name = tc_coll_name
            tc_scene.collection.children.link(tc_coll)

            tc_ob_txt_title_orig = tc_coll.objects[tc_ob_txt_title_base]
            tc_ob_txt_title = tc_ob_txt_title_orig.copy()
            tc_ob_txt_title.name = tc_ob_txt_title_name
            tc_coll.objects.unlink(tc_ob_txt_title_orig)
            tc_coll.objects.link(tc_ob_txt_title)

            tc_ob_txt_title_data_orig = tc_ob_txt_title.data
            assert tc_ob_txt_title_data_orig.name == tc_ob_txt_title_base
            tc_ob_txt_title_data = tc_ob_txt_title_data_orig.copy()
            tc_ob_txt_title_data.name = tc_ob_txt_title_name
            tc_ob_txt_title.data = tc_ob_txt_title_data

            tc_ob_txt_speakers_orig = tc_coll.objects[tc_ob_txt_speakers_base]
            tc_ob_txt_speakers = tc_ob_txt_speakers_orig.copy()
            tc_ob_txt_speakers.name = tc_ob_txt_speakers_name
            tc_coll.objects.unlink(tc_ob_txt_speakers_orig)
            tc_coll.objects.link(tc_ob_txt_speakers)

            tc_ob_txt_speakers_data_orig = tc_ob_txt_speakers.data
            assert tc_ob_txt_speakers_data_orig.name == tc_ob_txt_speakers_base
            tc_ob_txt_speakers_data = tc_ob_txt_speakers_data_orig.copy()
            tc_ob_txt_speakers_data.name = tc_ob_txt_speakers_name
            tc_ob_txt_speakers.data = tc_ob_txt_speakers_data
        else:
            tc_scene = bpy.data.scenes[tc_scene_name]
            tc_coll = tc_scene.collection.children[tc_coll_name]
            tc_ob_txt_title = tc_coll.objects[tc_ob_txt_title_name]
            tc_ob_txt_title_data = tc_ob_txt_title.data
            assert tc_ob_txt_title_data.name == tc_ob_txt_title_name
            tc_ob_txt_speakers = tc_coll.objects[tc_ob_txt_speakers_name]
            tc_ob_txt_speakers_data = tc_ob_txt_speakers.data
            assert tc_ob_txt_speakers_data.name == tc_ob_txt_speakers_name

        tc_ob_txt_title_data.body = self.bl_talk.title
        tc_ob_txt_speakers_data.body = self.bl_talk.speakers

        if self.bl_talk.title_card_render_frame == 0:
            self.bl_talk.title_card_render_frame = random.randrange(1, int(TITLECARD_LENGTH * 25.0))

        self.finalize()


class TaskProcessingInitVSE(Task):
    def __init__(self, stage, scene, bl_talk):
        super().__init__(stage)
        self.scene = scene
        self.bl_talk = bl_talk

    def start(self):
        super().start()

        vse = self.scene.sequence_editor
        if vse is None:
            vse = self.scene.sequence_editor_create()
        bl_talk = self.bl_talk

        # Need this info to not let current talk displayed in VSE if not the active one.
        bl_talk_active_uid = self.scene.bcon_video_data.talks[self.scene.bcon_video_data.talk_active_index].uid
        prev_meta_display = vse.meta_stack[-1] if vse.meta_stack else None

        vse.display_stack(None)

        # Find or assign a channel to the talk.
        if bl_talk.uid not in vse.channels:
            for channel in vse.channels[1:]:
                if not channel.lock:
                    channel.lock = True
                    channel.mute = False
                    channel.name = bl_talk.uid
                    break
        channel = vse.channels[bl_talk.uid]
        assert channel.lock
        channel_idx = (i for i, c in enumerate(vse.channels) if c == channel).__next__()

        # Find or create the meta-strip.
        if bl_talk.uid not in vse.sequences:
            vse.sequences.new_meta(bl_talk.uid, channel_idx, 1)
        meta = vse.sequences[bl_talk.uid]
        assert meta.type == 'META'

        # Update meta strip content according to bl_talk data.
        meta_frame_start = 100
        meta_channel_idx = 1
        for rvid in bl_talk.raw_videos:
            if not rvid.is_valid:
                continue
            rvid_id = rvid.filename
            if rvid_id not in meta.sequences:
                rvid_seq = meta.sequences.new_meta(rvid_id, meta_channel_idx, meta_frame_start)
                mv_seq = rvid_seq.sequences.new_movie(rvid_id, rvid.path_work, 2, meta_frame_start)
                ma_seq = rvid_seq.sequences.new_sound(rvid_id, rvid.path_work, 1, meta_frame_start)
                rvid_seq.frame_final_duration = mv_seq.frame_duration
            else:
                rvid_seq = meta.sequences[rvid_id]

            if rvid.is_cut_in_set:
                rvid_seq.frame_offset_start = rvid.cut_in
                rvid_seq.frame_start = meta_frame_start - rvid.cut_in
            if rvid.is_cut_out_set:
                rvid_seq.frame_offset_end = rvid_seq.frame_duration - rvid.cut_out

            rvid.cut_in = rvid_seq.frame_offset_start
            rvid.cut_out = rvid_seq.frame_duration - rvid_seq.frame_offset_end
            meta_channel_idx = rvid_seq.channel + 1
            meta_frame_start = rvid_seq.frame_final_end

        meta.frame_final_duration = meta_frame_start

        # Try not to replace currently shown meta in VSE if not processing the active talk.
        if bl_talk_active_uid == bl_talk.uid:
            vse.display_stack(meta)
        else:
            vse.display_stack(prev_meta_display)

        self.finalize()


class TaskProcessingUserSetTimings(TaskAsync):
    is_stage_blocking = True

    # This one is just waiting until user has defined cut-in/-out points in all acquired raw videos.
    def __init__(self, stage, bl_talk):
        super().__init__(stage, executor=None)
        self.bl_talk = bl_talk

    def start(self):
        super().start()
        self.update()

    def update(self):
        for rvid in self.bl_talk.raw_videos:
            if not rvid.is_final_set and not (rvid.is_cut_in_set and rvid.is_cut_out_set):
                return super().update()

        self.finalize()
        return super().update()


class TaskProcessingFinalizeVSE(Task):
    def __init__(self, stage, scene, bl_talk):
        super().__init__(stage)
        self.scene = scene
        self.bl_talk = bl_talk

    def start(self):
        super().start()
        import bpy

        vse = self.scene.sequence_editor
        assert vse is not None
        bl_talk = self.bl_talk

        # Need this info to not let current talk displayed in VSE if not the active one.
        bl_talk_active_uid = self.scene.bcon_video_data.talks[self.scene.bcon_video_data.talk_active_index].uid
        prev_meta_display = vse.meta_stack[-1] if vse.meta_stack else None

        meta = vse.sequences[bl_talk.uid]
        assert meta.type == 'META'

        vse.display_stack(meta)

        tc_scene_id = TaskProcessingCreateTitlecard.gen_task_name(TaskProcessingCreateTitlecard.TC_SCENE_BASE_NAME, bl_talk)
        if tc_scene_id not in meta.sequences:
            tc_seq = meta.sequences.new_scene(tc_scene_id, bpy.data.scenes[tc_scene_id], len(bl_talk.raw_videos) + 2, 1)
        else:
            tc_seq = meta.sequences[tc_scene_id]
            tc_seq.channel = len(bl_talk.raw_videos) + 1
            assert tc_seq.type == 'SCENE'
            assert tc_seq.scene == bpy.data.scenes[tc_scene_id]
        tc_seq.frame_final_duration = int((TITLECARD_LENGTH + TITLECARD_XFADE) * FPS)
        tc_seq.blend_type = 'ALPHA_OVER'

        frame_start = int(TITLECARD_LENGTH * FPS) + 1
        frame_cross_length = int(TITLECARD_XFADE * FPS)
        channel_idx = 1
        for rvid in bl_talk.raw_videos:
            if not rvid.is_valid:
                continue
            rvid_id = rvid.filename
            assert rvid_id in meta.sequences
            rvid_seq = meta.sequences[rvid_id]

            rvid_seq.channel = channel_idx

            if rvid.is_final_set:
                rvid_seq.frame_offset_start = rvid.final_cut_in
                rvid_seq.frame_start = rvid.final_start
                rvid_seq.frame_offset_end = rvid_seq.frame_duration - rvid.final_cut_out
            else:
                rvid_final_cut_in = rvid.cut_in - frame_cross_length
                # Do not allow negative final cut-in, this makes things complicated - and ugly one way or the other.
                # So instead, force-push forward the stored cut-in value.
                if rvid_final_cut_in < 0.0:
                    rvid.cut_in -= rvid_final_cut_in
                    rvid_final_cut_in = 0.0
                rvid_seq.frame_offset_start = rvid_final_cut_in
                rvid_seq.frame_start = frame_start - rvid_final_cut_in
                rvid_seq.frame_offset_end = rvid_seq.frame_duration - rvid.cut_out

                rvid.final_cut_in = rvid_seq.frame_offset_start
                rvid.final_start = rvid_seq.frame_start
                rvid.final_cut_out = rvid_seq.frame_duration - rvid_seq.frame_offset_end
                rvid.is_final_set = True

            channel_idx = rvid_seq.channel + 1
            frame_start = rvid_seq.frame_final_end
            frame_cross_length = 0

        # Try not to replace currently shown meta in VSE if not processing the active talk.
        if bl_talk_active_uid == bl_talk.uid:
            vse.display_stack(meta)
        else:
            vse.display_stack(prev_meta_display)

        self.finalize()


class TaskProcessingUserSetThumbnail(TaskAsync):
    is_stage_blocking = True

    # This one is just waiting until user has defined thumbnail frame in final video.
    def __init__(self, stage, bl_talk):
        super().__init__(stage, executor=None)
        self.bl_talk = bl_talk

    def start(self):
        super().start()
        self.update()

    def update(self):
        if self.bl_talk.thumbnail_frame > 0:
            self.finalize()
        return super().update()


class TaskExportingFFProbe(TaskAsync):
    is_stage_blocking = True

    def __init__(self, stage, executor, rvid_path):
        super().__init__(stage, executor)
        self.rvid_path = rvid_path

    @staticmethod
    def async_ffprobe(reports, rvid_path):
        ffprobe_iframes_cmd = (
            "ffprobe",
            "-print_format", "json",
            "-select_streams", "v",
            "-skip_frame", "nokey",
            "-show_frames",
            "-show_entries", "frame=pts_time,pict_type",
            rvid_path,
        )
        ffprobe_iframes_result = subprocess.run(ffprobe_iframes_cmd, capture_output=True)
        ffprobe_iframes_json = ffprobe_iframes_result.stdout
        # ffprobe cannot output valid JSON, needs to be cleaned up. :(
        ffprobe_iframes_json = b"{" + ffprobe_iframes_json[ffprobe_iframes_json.find(b'"frames":'):]
        ffprobe_iframes_data = json.loads(ffprobe_iframes_json)
        return ffprobe_iframes_data

    def start(self):
        if not self.executor:
            return
        super().start()
        self.future = self.executor.submit(self.async_ffprobe, self.reports, self.rvid_path)


class TaskExportingPrepare(Task):
    # WARNING: This code currently assumes there is a single raw video file!
    def __init__(self, stage, bl_talk, scene, export_settings):
        super().__init__(stage)
        self.scene = scene
        self.bl_talk = bl_talk
        self.export_settings = export_settings

    @staticmethod
    def get_rvid_paths(bl_talk):
        return (bl_talk.raw_videos[0].path_work,)

    def start(self):
        super().start()
        import bpy

        vse = self.scene.sequence_editor
        assert vse is not None
        bl_talk = self.bl_talk
        bl_talk.is_export_stage_validated = False
        bl_talk.is_export_stage_validation_required = False

        meta = vse.sequences[bl_talk.uid]
        assert meta.type == 'META'

        bl_bcon_video_data = self.scene.bcon_video_data

        # Need this info to not let current talk displayed in VSE if not the active one.
        bl_talk_active_uid = bl_bcon_video_data.talks[bl_bcon_video_data.talk_active_index].uid
        prev_meta_display = vse.meta_stack[-1] if vse.meta_stack else None

        vse.display_stack(meta)

        # Find timestamp (in seconds) of nearest I-frames after end of titlecard in final video.
        # This is where it can be 'cut', such that the titlecard part is re-encoded, while the main part is re-exported
        # without re-encoding the video. Both can then be stiched back together without any re-encoding, while keeping
        # a good-enough precision.

        rvid_id = bl_talk.raw_videos[0].filename
        rvid_seq = meta.sequences[rvid_id]
        rvid_path = self.get_rvid_paths(bl_talk)[0]

        # Temp blendfile used to render frames for the processed talk.
        blendfile_path = os.path.join(os.path.dirname(rvid_path), f"{bl_talk.uid}.blend")
        bpy.ops.wm.save_as_mainfile(filepath=blendfile_path, compress=True, copy=True)

        tc_scene_id = TaskProcessingCreateTitlecard.gen_task_name(TaskProcessingCreateTitlecard.TC_SCENE_BASE_NAME, bl_talk)
        tc_seq = meta.sequences[tc_scene_id]

        ffprobe_iframes_data = self.export_settings.ffprobe_iframes_data

        # Exact frame where cut could be made in raw video, where the fade-in from titlecard ends.
        rvid_cut_exact = (tc_seq.frame_final_end - rvid_seq.frame_start) / FPS
        rvid_cut_best_iframe = rvid_cut_exact
        iframe_timestamp_prev = 0.0
        for iframe in ffprobe_iframes_data["frames"]:
            iframe_timestamp = float(iframe["pts_time"])
            if iframe_timestamp < iframe_timestamp_prev:
                print(f"WARNING: {rvid_path} contains non-monotonic I-Frames, final result will likely not be good!")
            iframe_timestamp_prev = iframe_timestamp
            if iframe_timestamp >= rvid_cut_exact:
                rvid_cut_best_iframe = iframe_timestamp
                break

        # Store all these data in the Exporting stage, for the other, actually exporting, tasks.
        work_path = os.path.dirname(rvid_path)
        self.export_settings.rvid_raw_path = rvid_path

        self.export_settings.blendfile_path = blendfile_path

        self.export_settings.thumbnail_frame = bl_talk.thumbnail_frame
        self.export_settings.thumbnail_render_path_template = os.path.join(work_path, f"{bl_talk.uid}_thumbnail_####.jpg")
        self.export_settings.thumbnail_render_path = os.path.join(work_path, f"{bl_talk.uid}_thumbnail_{self.export_settings.thumbnail_frame:04}.jpg")

        self.export_settings.tc_frame = bl_talk.title_card_render_frame
        self.export_settings.tc_render_path_template = os.path.join(work_path, f"{bl_talk.uid}_titlecard_####.png")
        self.export_settings.tc_render_path = os.path.join(work_path, f"{bl_talk.uid}_titlecard_{self.export_settings.tc_frame:04}.png")

        self.export_settings.rvid_tc_path = os.path.join(work_path, f"{bl_talk.uid}_titlecard.mkv")
        self.export_settings.rvid_tc_duration = (tc_seq.frame_final_duration - (tc_seq.frame_final_end - rvid_seq.frame_final_start)) / FPS
        self.export_settings.rvid_tc_xfade_duration = (tc_seq.frame_final_end - rvid_seq.frame_final_start) / FPS
        self.export_settings.rvid_tc_mvid_trim_start = (tc_seq.frame_final_start - rvid_seq.frame_start) / FPS
        self.export_settings.rvid_tc_mvid_trim_end = rvid_cut_best_iframe
        if self.export_settings.rvid_tc_mvid_trim_start < 0.0:
            # Need to compensate the extra length added by the padding at the start of the main video.
            self.export_settings.rvid_tc_mvid_trim_end -= self.export_settings.rvid_tc_mvid_trim_start

        self.export_settings.rvid_main_path = os.path.join(work_path, f"{bl_talk.uid}_main.mkv")
        self.export_settings.rvid_main_start = rvid_cut_best_iframe
        self.export_settings.rvid_main_duration = ((rvid_seq.frame_final_end - rvid_seq.frame_start) / FPS) - rvid_cut_best_iframe

        self.export_settings.final_concat_path = os.path.join(work_path, f"{bl_talk.uid}_concat.txt")
        self.export_settings.final_path = os.path.join(work_path, f"{bl_talk.uid}_final.mkv")

        if bl_bcon_video_data.paths.backup_dir:
            self.export_settings.final_backup_path = os.path.join(bl_bcon_video_data.paths.backup_dir,
                                                                  os.path.basename(self.export_settings.final_path))
        else:
            self.export_settings.final_backup_path = ""

        print(self.export_settings)

        # Try not to replace currently shown meta in VSE if not processing the active talk.
        if bl_talk_active_uid == bl_talk.uid:
            vse.display_stack(meta)
        else:
            vse.display_stack(prev_meta_display)

        self.finalize()


class TaskExportingRenderFrame(TaskAsync):
    is_stage_blocking = True

    def __init__(self, stage, executor, blendfile_path, render_path, render_format, frame):
        super().__init__(stage, executor)
        self.blendfile_path = blendfile_path
        self.render_path = render_path
        self.render_format = render_format
        self.frame = frame

    @staticmethod
    def async_render(reports, blender_path, blendfile_path, render_path, render_format, frame):
        args = (
            blender_path,
            "--background",
            blendfile_path,
            "--render-format", render_format,
            "--engine", "BLENDER_EEVEE_NEXT",
            "--render-output", render_path,
            "--render-frame", str(frame),
        )
        result = subprocess.run(args, capture_output=True)
        return result

    def start(self):
        if not self.executor:
            return
        super().start()
        import bpy
        self.future = self.executor.submit(self.async_render, self.reports, bpy.app.binary_path,
                                           self.blendfile_path, self.render_path, self.render_format, self.frame)


class TaskExportingFFMPEGVideo(TaskAsync):
    is_stage_blocking = True

    def __init__(self, stage, executor, export_settings):
        super().__init__(stage, executor)
        self.export_settings = export_settings

    @classmethod
    def async_ffmpeg_export(cls, reports, export_settings):
        return None

    def start(self):
        if not self.executor:
            return
        super().start()
        import bpy
        self.future = self.executor.submit(self.async_ffmpeg_export, self.reports, self.export_settings)


class TaskExportingTitleCardVideo(TaskExportingFFMPEGVideo):
    def __init__(self, stage, executor, export_settings):
        super().__init__(stage, executor, export_settings)

    # When main video needs to be trimmed (the raw video 'starts before' the 0.0 timestamp).
    FFMPEG_FILTER_TRIM = """
        [0:v] trim=start={mvid_trim_start}:end={mvid_trim_end}, setpts=PTS-STARTPTS [vid_main];
        [1:v] format=pix_fmts=yuva420p, fade=type=out:start_time={tc_duration}:duration={xfade_duration}:alpha=1, trim=end={tc_trim_end}, setpts=PTS-STARTPTS [vid_titlecard];
        [vid_main][vid_titlecard] overlay [vid];
        [0:a] atrim=start={mvid_trim_start}:end={mvid_trim_end}, asetpts=PTS-STARTPTS, afade=type=in:start_time={tc_duration}:duration={xfade_duration} [aud];
    """
    # When main video needs to be padded (the raw video 'starts after' the 0.0 timestamp). Not a common case.
    FFMPEG_FILTER_PAD = """
        [0:v] tpad=start_duration={mvid_pad_start}:start_mode=add, trim=end={mvid_trim_end}, setpts=PTS-STARTPTS [vid_main];
        [1:v] format=pix_fmts=yuva420p, fade=type=out:start_time={tc_duration}:duration={xfade_duration}:alpha=1, trim=end={tc_trim_end}, setpts=PTS-STARTPTS [vid_titlecard];
        [vid_main][vid_titlecard] overlay [vid];
        [0:a] adelay=delays={maud_pad_start}:all=true, atrim=end={mvid_trim_end}, asetpts=PTS-STARTPTS, afade=type=in:start_time={tc_duration}:duration={xfade_duration} [aud];
    """

    @classmethod
    def async_ffmpeg_export(cls, reports, export_settings):
        # FFMPEG complex filter, mainly performs the cross-fade between the title card and the main video
        # (and the fade-in of the main video audio at the same time).
        # See `ffmpeg_cmd.txt` file for detailed explanations of these commands.
        ffmpeg_filter = ""
        # When to cut titlecard video stream, with some little extra to ensure we end up with a completely transparent
        # frame, since the overlay filter seems to keep the last frame 'forever' overlayed over the main video.
        tc_trim_end = export_settings.rvid_tc_duration + export_settings.rvid_tc_xfade_duration + 0.1
        if export_settings.rvid_tc_mvid_trim_start < 0.0:
            ffmpeg_filter = cls.FFMPEG_FILTER_PAD.format(
                mvid_pad_start=-export_settings.rvid_tc_mvid_trim_start,
                # audio delay is in milliseconds!
                maud_pad_start=(-export_settings.rvid_tc_mvid_trim_start - AUDIO_PRIMING_SAMPLE_CORRECTION) * 1000.0, 
                mvid_trim_end=export_settings.rvid_tc_mvid_trim_end,
                tc_duration=export_settings.rvid_tc_duration,
                xfade_duration=export_settings.rvid_tc_xfade_duration,
                tc_trim_end=tc_trim_end
            )
        else:
            ffmpeg_filter = cls.FFMPEG_FILTER_TRIM.format(
                mvid_trim_start=export_settings.rvid_tc_mvid_trim_start,
                mvid_trim_end=export_settings.rvid_tc_mvid_trim_end,
                tc_duration=export_settings.rvid_tc_duration,
                xfade_duration=export_settings.rvid_tc_xfade_duration,
                tc_trim_end=tc_trim_end
            )

        args = (
            "ffmpeg",
            "-stats",
            "-v", "warning",
            "-i", export_settings.rvid_raw_path,
            "-loop", "1",
            "-i", export_settings.tc_render_path,
            "-filter_complex", ffmpeg_filter,
            "-map", "[vid]",
            "-map", "[aud]",
            "-c:v", "libx264",
            "-profile:v", "high",
            "-preset", "fast",
            "-crf", "23",
            "-g", "25",
            "-c:a", "pcm_s16le",
            "-y", export_settings.rvid_tc_path,
        )
        pprint(args)
        result = subprocess.run(args, capture_output=True)
        return result


class TaskExportingMainVideo(TaskExportingFFMPEGVideo):
    def __init__(self, stage, executor, export_settings):
        super().__init__(stage, executor, export_settings)

    @classmethod
    def async_ffmpeg_export(cls, reports, export_settings):
        args = (
            "ffmpeg",
            "-stats",
            "-v", "warning",
            "-ss", str(export_settings.rvid_main_start),
            "-i", export_settings.rvid_raw_path,
            # Here too the 'priming sample' needs to be trimmed away...
            "-filter:a", f"[in] atrim=start={AUDIO_PRIMING_SAMPLE_CORRECTION} [out]",
            "-c:v", "copy",
            "-c:a", "pcm_s16le",
            "-t", str(export_settings.rvid_main_duration),
            "-y", export_settings.rvid_main_path,
        )
        # ~ pprint(args)
        result = subprocess.run(args, capture_output=True)
        return result


class TaskExportingFinalVideo(TaskExportingFFMPEGVideo):
    def __init__(self, stage, executor, export_settings):
        super().__init__(stage, executor, export_settings)

    @classmethod
    def async_ffmpeg_export(cls, reports, export_settings):
        with open(export_settings.final_concat_path, 'wt') as concat_f:
            concat_f.write(f"file {export_settings.rvid_tc_path}\nfile {export_settings.rvid_main_path}\n")

        args = (
            "ffmpeg",
            "-stats",
            "-v", "warning",
            "-f", "concat",
            "-safe", "0",
            "-i", export_settings.final_concat_path,
            "-c:v", "copy",
            "-c:a", "aac",
            "-b:a", "128k",
            "-y", export_settings.final_path,
        )
        result = subprocess.run(args, capture_output=True)
        return result


class TaskExportingUserValidate(TaskAsync):
    is_stage_blocking = True

    # This one is just waiting until user has validated the final export result.
    def __init__(self, stage, bl_talk, export_settings):
        super().__init__(stage, executor=None)
        self.bl_talk = bl_talk
        self.export_settings = export_settings

    def start(self):
        super().start()
        self.bl_talk.export_path = self.export_settings.final_path
        self.bl_talk.thumbnail_path = self.export_settings.thumbnail_render_path
        self.bl_talk.is_export_stage_validation_required = True
        self.update()
    
    def update(self):
        if self.bl_talk.is_export_stage_validated:
            self.bl_talk.is_export_stage_validation_required = False
            self.bl_talk.export_backup_path = self.export_settings.final_backup_path
            self.finalize()

        return super().update()


class TaskUploadingYoutube(TaskAsync):
    is_stage_blocking = True

    # This one is just waiting until user has validated the final export result.
    def __init__(self, stage, executor, bl_talk):
        super().__init__(stage, executor)
        self.bl_talk = bl_talk

    def start(self):
        if not self.executor:
            return
        super().start()
        from . import upload_youtube
        upload_video = upload_youtube.UploadVideo(self.bl_talk)
        self.future = self.executor.submit(upload_video.upload_video, self.reports)


class TaskUploadingUserValidate(TaskAsync):
    is_stage_blocking = True

    # This one is just waiting until user has validated the final upload result.
    def __init__(self, stage, bl_talk):
        super().__init__(stage, executor=None)
        self.bl_talk = bl_talk

    def start(self):
        super().start()
        self.bl_talk.is_upload_stage_validation_required = True
        self.update()
    
    def update(self):
        if self.bl_talk.is_upload_stage_validated:
            self.bl_talk.is_upload_stage_validation_required = False
            self.finalize()

        return super().update()


class TaskDoneCleanupBlender(Task):
    # NOTE: Only clear the VSE from main scene. TitleCard scenes are kept for the time being.

    def __init__(self, stage, scene, bl_talk):
        super().__init__(stage)
        self.scene = scene
        self.bl_talk = bl_talk

    def start(self):
        super().start()

        vse = self.scene.sequence_editor
        if vse is None:
            return  # Hrrrrm... very unlikely, could even error here.
        bl_talk = self.bl_talk

        # Need this info to not let current talk displayed in VSE if not the active one.
        bl_talk_active_uid = self.scene.bcon_video_data.talks[self.scene.bcon_video_data.talk_active_index].uid
        prev_meta_display = vse.meta_stack[-1] if vse.meta_stack else None

        vse.display_stack(None)

        # Find the channel of the talk and clean it.
        if bl_talk.uid in vse.channels:
            channel = vse.channels[bl_talk.uid]
            channel.lock = False
            channel.mute = True
            channel.name = "Channel"

        # Find the meta-strip of the talk and delete it.
        if bl_talk.uid in vse.sequences:
            meta = vse.sequences[bl_talk.uid]
            assert meta.type == 'META'
            vse.sequences.remove(meta)

        # Try not to replace currently shown meta in VSE if not processing the active talk.
        if bl_talk_active_uid == bl_talk.uid:
            vse.display_stack(None)
        else:
            vse.display_stack(prev_meta_display)

        self.finalize()


class TaskDoneFinalizing(TaskAsync):
    is_stage_blocking = True

    # This one is just waiting until all other non-blocking async tasks from other stages are done.
    def __init__(self, stage):
        super().__init__(stage, executor=None)

    def start(self):
        super().start()
        self.update()
    
    def update(self):
        if any(stage.has_active_nonblocking_async_tasks for stage in self.stage.talk.stages):
            return super().update()

        for stage in self.stage.talk.stages:
            if stage == self.stage:
                continue
            if stage.tasks:
                print(f"Stage {stage!r} still has running blocking tasks ({stage.tasks})!")
                assert False

        self.finalize()
        return super().update()


class Stage:
    """
    The stage management of a talk. This is an interface, each actual stage must implement it.
    
    A stage defines a logical set of tasks that can be executed in parallel, without (strong) dependency between them.
    
    However, to be able to move to the next stage, a set of conditions must be met:
      * All non-asyncrhonous tasks must be finished.
      * All asynchronous blocking tasks must be finished.
      * All asynchronous non-blocking tasks must be started.
    """
    identifier = ""

    statuses = {
        STATUS_READY: 0,
        STATUS_PENDING: 1,
        STATUS_ACTIVE: 2,
        STATUS_FINALIZING: 3,
        STATUS_DONE: 4,
        STATUS_ERROR: 100,
    }

    def __init__(self, talk, executor, prev_stage):
        self.reports = reports.ReportsHandler()
        self.executor = executor
        self.prev_stage = prev_stage
        self.talk = talk
        self.tasks = []
        self._status = STATUS_READY
        self.status_message = ""

    def reset(self):
        for task in self.tasks:
            task.cancel()
        self.tasks = []
        self.reports.clear()
        # Only time where status can be forced out of ERROR.
        self._status = STATUS_READY

    def start(self):
        if self.prev_stage is None:
            self.status = STATUS_ACTIVE
        elif self.prev_stage.status == STATUS_FINALIZING:
            self.status = STATUS_PENDING
        elif self.prev_stage.status == STATUS_DONE:
            self.status = STATUS_ACTIVE
        else:
            # Any other status of prev stage does not allow to process current one.
            self.reset()

    def finalize(self, bl_context, bl_talk, do_wait=False, do_update=True):
        if do_update:
            self.update(bl_context, bl_talk)
        if self.status != STATUS_ACTIVE:
            return

        can_finalize = True
        need_finalizing = False
        for task in self.tasks:
            if task.is_async:
                if task.is_stage_blocking and not need_finalizing:
                    need_finalizing = True
            else:
                if do_wait:
                    # Allowed to block and wait.
                    task.finalize()
                else:
                    # No waiting allowed, cannot go into FINALIZING status.
                    can_finalize = False
        if can_finalize:
            self.status = STATUS_FINALIZING if need_finalizing else STATUS_DONE

    def update(self, bl_context, bl_talk):
        assert(bl_talk.uid == self.talk.uid)

        prev_stage_status = STATUS_DONE if self.prev_stage is None else self.prev_stage.status
        if prev_stage_status == STATUS_DONE and self.status in {STATUS_READY, STATUS_PENDING}:
            self.status = STATUS_ACTIVE

        bl_talk.is_stage_pending = (self.status == STATUS_PENDING)
        bl_talk.is_stage_error = (self.status == STATUS_ERROR)
        if self.status not in {STATUS_ACTIVE, STATUS_FINALIZING, STATUS_DONE}:
            return

        # NOTE: Never force a task out of its ACTIVE status, this has to be done from the controlling code
        #       through a call to `finalize()`.
        need_active = (self.status == STATUS_ACTIVE)
        need_finalizing = False
        for task in self.tasks:
            task.update()
            if task.status == STATUS_ERROR:
                self.reports.add_error("Error in task", task.reports)
                self.status = STATUS_ERROR
            elif task.status != STATUS_DONE:
                if task.is_async:
                    if task.is_stage_blocking and not need_finalizing:
                        need_finalizing = True
                else:
                    need_active = True
        if need_active:
            assert self.status == STATUS_ACTIVE
        elif need_finalizing:
            assert self.status in {STATUS_ACTIVE, STATUS_FINALIZING}
            self.status = STATUS_FINALIZING
        else:
            self.status = STATUS_DONE
        bl_talk.is_stage_error = (self.status == STATUS_ERROR)

    def next_stage_poll(self):
        return self.status in {STATUS_ACTIVE, STATUS_FINALIZING, STATUS_DONE}

    def prev_stage_poll(self):
        return True

    @property
    def status(self):
        return self._status
    
    @status.setter
    def status(self, status):
        assert status in self.statuses
        if self._status != STATUS_ERROR:
            self._status = status

    @property
    def has_active_sync_tasks(self):
        return any(not t.is_async and t.status is not STATUS_DONE for t in self.tasks)

    @property
    def has_active_blocking_async_tasks(self):
        return any(t.is_async and t.is_stage_blocking and t.status is not STATUS_DONE for t in self.tasks)

    @property
    def has_active_nonblocking_async_tasks(self):
        return any(t.is_async and not t.is_stage_blocking and t.status is not STATUS_DONE for t in self.tasks)


class StageUpcoming(Stage):
    identifier = STAGE_UPCOMING

    def __init__(self, talk, executor, prev_stage):
        assert prev_stage is None
        super().__init__(talk, executor, prev_stage)
        self._status = STATUS_DONE
        self.status_message = "Not yet started"

    def prev_stage_poll(self):
        return False

    def next_stage_poll(self):
        return True


class RawVideoData:
    """
    Data for a Raw video file.
    """
    def __init__(self, src_path="", work_path="", backup_paths=[]):
        self.src_path = src_path
        self.work_path = work_path
        self.backup_paths = backup_paths
        self.is_valid = ...
        self.mtime = ...
        self.size = ...

        self.stat_task = ...
        self.symlink_to_work_task = ...
        self.copy_to_work_task = None  # instead of ... , effectively disable this step.
        self.copy_to_backup_tasks = None  # instead of ... , effectively disable this step.

    def cancel(self):
        for task in (self.stat_task, self.symlink_to_work_task, self.copy_to_work_task, self.copy_to_backup_tasks):
            if task not in {None, ...}:
                task.cancel()


class StageAcquiring(Stage):
    identifier = STAGE_ACQUIRING

    def __init__(self, talk, executor, prev_stage):
        super().__init__(talk, executor, prev_stage)
        self.reset()

    def reset(self):
        # A mapping between raw video identifier (their destination filename), and their data.
        self.raw_vids = {}
        self.status_message = ""
        super().reset()

    def update(self, bl_context, bl_talk):
        if self.status == STATUS_PENDING:
            super().update(bl_context, bl_talk)
            return
        unused_raw_vids = set(self.raw_vids.keys())
        for bl_rvid in bl_talk.raw_videos:
            if bl_rvid.filename not in self.raw_vids:
                if self.status != STATUS_ACTIVE:
                    self.reports.add_warning(f"Cannot acquire new video files when Acquiring stage is in '{self.status}' status")
                    continue
                rvid_data = self.raw_vids[bl_rvid.filename] = RawVideoData(bl_rvid.path_src, bl_rvid.path_work, bl_rvid.paths_backup)
                if bl_rvid.is_valid:
                    rvid_data.is_valid = bl_rvid.is_valid
                    rvid_data.size = bl_rvid.size // 1024  # int32 is not large enough for +2Gb files.
                    rvid_data.mtime = bl_rvid.mtime
            else:
                rvid_data = self.raw_vids[bl_rvid.filename]
                unused_raw_vids.remove(bl_rvid.filename)

            if rvid_data.stat_task is not None:
                if rvid_data.stat_task is ...:
                    rvid_data.stat_task = TaskFileStatAsync(self, self.executor, rvid_data.src_path)
                    rvid_data.stat_task.initialize()
                    rvid_data.stat_task.start()
                    self.tasks.append(rvid_data.stat_task)
                if rvid_data.stat_task.status == STATUS_DONE:
                    statinfo = rvid_data.stat_task.future_result
                    bl_rvid.is_valid = rvid_data.is_valid = bool(stat.S_ISREG(statinfo.st_mode))
                    bl_rvid.mtime = rvid_data.mtime = statinfo.st_mtime
                    rvid_data.size = statinfo.st_size
                    bl_rvid.size = statinfo.st_size // 1024  # int32 is not large enough for +2Gb files.
                    self.tasks.remove(rvid_data.stat_task)
                    rvid_data.stat_task = None
                if rvid_data.is_valid is ...:
                    continue

            if not rvid_data.is_valid:
                self.reports.add_error("Invalid raw source video file", rvid_data.src_path)
                self._status = STATUS_ERROR
                continue

            # Skip copying to local dir, create symlink instead.
            if rvid_data.symlink_to_work_task is not None:
                if rvid_data.symlink_to_work_task is ...:
                    rvid_data.symlink_to_work_task = TaskFileSymlinkAsync(self, self.executor, rvid_data.src_path, rvid_data.work_path)
                    rvid_data.symlink_to_work_task.initialize()
                    rvid_data.symlink_to_work_task.start()
                    self.tasks.append(rvid_data.symlink_to_work_task)
                if rvid_data.symlink_to_work_task.status == STATUS_DONE:
                    symlink_reports = rvid_data.symlink_to_work_task.future_result
                    if symlink_reports.has_error:
                        # Retry?
                        continue
                    self.tasks.remove(rvid_data.symlink_to_work_task)
                    rvid_data.symlink_to_work_task = None
            continue

            if rvid_data.copy_to_work_task is not None:
                if rvid_data.copy_to_work_task is ...:
                    rvid_data.copy_to_work_task = TaskFileCopyAsync(self, self.executor, rvid_data.src_path, rvid_data.work_path)
                    rvid_data.copy_to_work_task.initialize()
                    rvid_data.copy_to_work_task.start()
                    self.tasks.append(rvid_data.copy_to_work_task)
                if rvid_data.copy_to_work_task.status == STATUS_DONE:
                    copy_reports = rvid_data.copy_to_work_task.future_result
                    if copy_reports.has_error:
                        # Retry?
                        continue
                    self.tasks.remove(rvid_data.copy_to_work_task)
                    rvid_data.copy_to_work_task = None

        # Remove raw videos that are not in given bl_talk anymore.
        for unused_filename in unused_raw_vids:
            rvid_data = self.raw_vids.pop(unused_filename)
            rvid_data.cancel()

        if self.raw_vids:
            self.status_message = (f"Acquiring {len(self.raw_vids)} videos..."
                                   if self.tasks else
                                   f"{len(self.raw_vids)} videos acquired, ready.")

        super().update(bl_context, bl_talk)

    def next_stage_poll(self):
        if not super().next_stage_poll():
            return False
        # Do not allow moving to next stage if no videos have been acquired.
        if not self.raw_vids:
            return False
        return True


class StageProcessing(Stage):
    identifier = STAGE_PROCESSING

    def __init__(self, talk, executor, prev_stage):
        super().__init__(talk, executor, prev_stage)
        self.reset()

    def reset(self):
        # Ensure title card for this talk exists, create it from default template if needed.
        # Note that user can further tweak it if required.
        self.create_titlecard_task = ...
        # Put acquired raw video(s) in own VSE meta strip.
        self.init_vse_task = ...
        # Wait for user to set cut-in and cut-out timestamps.
        self.user_set_timings_task = ...
        # Finish creating VSE editing (cut raw video as required, add title card...).
        self.finalize_vse_task = ...
        # Wait for user to set thumbnail timestamp.
        self.user_set_thumbnail_task = ...
        super().reset()

    def update(self, bl_context, bl_talk):
        if self.status == STATUS_PENDING:
            super().update(bl_context, bl_talk)
            return
        if self.create_titlecard_task is not None:
            if self.create_titlecard_task is ...:
                # Immediate task.
                self.create_titlecard_task = TaskProcessingCreateTitlecard(self, bl_talk)
                self.create_titlecard_task.initialize()
                self.create_titlecard_task.start()
                if self.create_titlecard_task.status is not STATUS_DONE:
                    self.reports.add_error("Create TitleCard task failed", self.create_titlecard_task.reports)
                self.create_titlecard_task = None

        if self.init_vse_task is not None:
            if self.init_vse_task is ...:
                # Immediate task.
                self.init_vse_task = TaskProcessingInitVSE(self, bl_context.scene, bl_talk)
                self.init_vse_task.initialize()
                self.init_vse_task.start()
                if self.init_vse_task.status is not STATUS_DONE:
                    self.reports.add_error("Init VSE task failed", self.init_vse_task.reports)
                self.init_vse_task = None

        # If user clears some cut in/out timestamps, reset the final part of this stage.
        if self.user_set_timings_task is None:
            for rvid in bl_talk.raw_videos:
                if not rvid.is_final_set:
                    self.user_set_timings_task = ...
                    self.finalize_vse_task = ...
                    self.user_set_thumbnail_task = ...
        if self.user_set_timings_task is not None:
            if self.user_set_timings_task is ...:
                self.user_set_timings_task = TaskProcessingUserSetTimings(self, bl_talk)
                self.user_set_timings_task.initialize()
                self.status_message = "Please set cut-in/-out timestamps for acquired raw videos."
                self.user_set_timings_task.start()
                self.tasks.append(self.user_set_timings_task)
            if self.user_set_timings_task.status == STATUS_DONE:
                self.tasks.remove(self.user_set_timings_task)
                self.user_set_timings_task = None

        if self.finalize_vse_task is not None:
            if self.finalize_vse_task is ...:
                # Immediate task.
                self.finalize_vse_task = TaskProcessingFinalizeVSE(self, bl_context.scene, bl_talk)
                self.tasks.append(self.finalize_vse_task)
            # Wait for user to set timings before doing final sequences layout.
            if self.user_set_timings_task is None and self.finalize_vse_task.status == STATUS_READY:
                self.finalize_vse_task.start()
                if self.finalize_vse_task.status is not STATUS_DONE:
                    self.reports.add_error("Finalize VSE task failed", self.finalize_vse_task.reports)
                self.tasks.remove(self.finalize_vse_task)
                self.finalize_vse_task = None

        if self.user_set_thumbnail_task is not None:
            if self.user_set_thumbnail_task is ...:
                self.user_set_thumbnail_task = TaskProcessingUserSetThumbnail(self, bl_talk)
                self.user_set_thumbnail_task.initialize()
                self.status_message = "Please choose a frame in the final video as thumbnail."
                self.user_set_thumbnail_task.start()
                self.tasks.append(self.user_set_thumbnail_task)
            if self.user_set_thumbnail_task.status == STATUS_DONE:
                self.tasks.remove(self.user_set_thumbnail_task)
                self.user_set_thumbnail_task = None
                self.status_message = "Ready for exporting stage."

        super().update(bl_context, bl_talk)

    def next_stage_poll(self):
        if not super().next_stage_poll():
            return False
        # Do not allow moving to next stage if waiting for user action.
        if self.user_set_thumbnail_task is not None:
            return False
        return True

    def finalize(self, bl_context, bl_talk, do_wait=False):
        super().finalize(bl_context, bl_talk, do_wait)

        # In case user did some tweaking to raw video strips, store one last time the final timings.
        vse = bl_context.scene.sequence_editor
        assert vse is not None

        meta = vse.sequences[bl_talk.uid]
        assert meta.type == 'META'

        for rvid in bl_talk.raw_videos:
            if not rvid.is_valid:
                continue
            rvid_id = rvid.filename
            assert rvid_id in meta.sequences
            rvid_seq = meta.sequences[rvid_id]

            rvid.final_cut_in = rvid_seq.frame_offset_start
            rvid.final_start = rvid_seq.frame_start
            rvid.final_cut_out = rvid_seq.frame_duration - rvid_seq.frame_offset_end
            rvid.is_final_set = True


class StageExporting(Stage):
    identifier = STAGE_EXPORTING

    # WARNING: This whole stage currently assumes that there is a single raw video file!

    class ExportSettings:
        __slots__ = (
            # Path of the main (input) raw video.
            "rvid_raw_path",
            # Timestamps of IFrames in main video, as returned by ffprobe.
            "ffprobe_iframes_data",

            # Path to the talk-spacific blendfile from which still images will be rendered.
            "blendfile_path",

            ### Thumbnail image rendering.
            # Frame to render.
            "thumbnail_frame",
            # Path template to pass to Blender background rendering (allways adds frame number...).
            "thumbnail_render_path_template",
            # Expected final path of the rendered image.
            "thumbnail_render_path",

            ### TitleCard image rendering.
            # Frame to render.
            "tc_frame",
            # Path template to pass to Blender background rendering (allways adds frame number...).
            "tc_render_path_template",
            # Expected final path of the rendered image.
            "tc_render_path",

            ### TitleCard sequence, including cross-fade to the main video.
            # Duration of the titlecard (excluding the crossfade effect duration).
            "rvid_tc_duration",
            # Duration of the crossfade effect.
            "rvid_tc_xfade_duration",
            # Start timestamp in the raw input main video, matching the start timestamp of the titelcard.
            # Can be negative, in which case export task will pad the main video/audio instead of trimming them.
            "rvid_tc_mvid_trim_start",
            # End timestamp in the raw input main video, should always be on one of its I-Frames.
            "rvid_tc_mvid_trim_end",
            # Output path for the encoded titlecard video.
            "rvid_tc_path",

            ### Remaining of the main video sequence.
            # Start timestamp in the raw input main video.
            # Note: Processed through the `-ss` demuxer parameter of FFMPEG, so only accurate if on an I-Frame.
            "rvid_main_start",
            # Duration of the remaining of the main video to copy.
            # Note: Processed through the `-t` muxer parameter of FFMPEG, so may not be perfectly accurate.
            "rvid_main_duration",
            # Output path for the 'copied', trimmed remaining of the main video.
            "rvid_main_path",

            ### Final video sequence.
            # Path to FFMPEG 'concat' file listing all intermediary sequences to concatenate together.
            "final_concat_path",
            # Output final video path.
            "final_path",
            # Output final video backup path, may be empty.
            "final_backup_path",
        )

        def __repr__(self):
            ret = [str(self.__class__)]
            for param_id in self.__slots__:
                if param_id == "ffprobe_iframes_data":
                    ret.append(f"\t{param_id+':': <28}{len(self.ffprobe_iframes_data['frames'])} iframes found")
                else:
                    ret.append(f"\t{param_id+':': <28}" + repr(getattr(self, param_id)))
            return "\n".join(ret)

    def __init__(self, talk, executor, prev_stage):
        super().__init__(talk, executor, prev_stage)
        self.reset()

    def reset(self):
        # Query main video files for its I-frames.
        # NOTE: Annoying, but this ffprobe can take long on big files, so needs to be in its own async task.
        #       (rest of preparing task needs access to Blender data, so needs to be in immediate task).
        self.ffprobe_task = ...
        # Prepare all required settings (paths, timings...) for the various export tasks.
        self.prepare_task = ...
        # Render the thumbnail for youtube preview.
        self.render_thumbnail_task = ...
        # Render the title card image.
        self.render_titlecard_task = ...
        # Export the title card intro sequence, with crossfade to the start of the main video.
        self.export_titlecard_sequence_task = ...
        # Export the remaining of the main video.
        self.export_main_sequence_task = ...
        # Export the merged final video from the titlecard and main sequences.
        self.export_final_task = ...
        # Request user for validation of the final export.
        self.export_user_validate_task = ...

        self.export_settings = self.ExportSettings()
        super().reset()

    def update(self, bl_context, bl_talk):
        if self.status == STATUS_PENDING:
            super().update(bl_context, bl_talk)
            return
        if IS_DEBUG:
            if bl_talk.skip_stage_export:
                # Fully by-pass this stage.
                self.ffprobe_task = None
                self.prepare_task = None
                self.render_thumbnail_task = None
                self.render_titlecard_task = None
                self.export_titlecard_sequence_task = None
                self.export_main_sequence_task = None
                self.export_final_task = None
                self.export_user_validate_task = None
                bl_talk.is_export_stage_validation_required = False
                bl_talk.is_export_stage_validated = True
                self.status_message = "Exporting stage skipped as requested, ready for uploading stage."

        if self.ffprobe_task is not None:
            ffprobe_task = self.ffprobe_task
            if ffprobe_task is ...:
                ffprobe_task = TaskExportingFFProbe(self, self.executor, TaskExportingPrepare.get_rvid_paths(bl_talk)[0])
                self.ffprobe_task = ffprobe_task
                ffprobe_task.initialize()
                self.tasks.append(ffprobe_task)
                ffprobe_task.start()
            if ffprobe_task.status == STATUS_ACTIVE:
                self.status_message = "Probing raw videos for key-frames..."
            if ffprobe_task.status == STATUS_DONE:
                ffprobe_result = ffprobe_task.future_result
                self.export_settings.ffprobe_iframes_data = ffprobe_result
                pprint(ffprobe_result)
                print("\n\n")
                self.tasks.remove(ffprobe_task)
                self.ffprobe_task = None
                self.status_message = "Probing raw videos for key-frames... Done."

        if self.prepare_task is not None:
            prepare_task = self.prepare_task
            if prepare_task is ...:
                prepare_task = TaskExportingPrepare(self, bl_talk, bl_context.scene, self.export_settings)
                self.prepare_task = prepare_task
                prepare_task.initialize()
                self.tasks.append(prepare_task)
            # Wait for ffprobe task to be done for actually starting.
            if self.ffprobe_task is None and prepare_task.status == STATUS_READY:
                # Immediate task.
                prepare_task.start()
                if prepare_task.status is not STATUS_DONE:
                    self.reports.add_error("Update TitleCard task failed", prepare_task.reports)
                self.tasks.remove(prepare_task)
                self.prepare_task = None

        if self.prepare_task is not None:
            # Hard-break here, cannot init the other rendering/exporting tasks without fully prepared export settings data.
            super().update(bl_context, bl_talk)
            return

        if self.export_main_sequence_task is not None:
            mvid_seq_task = self.export_main_sequence_task
            if mvid_seq_task is ...:
                mvid_seq_task = TaskExportingMainVideo(self, self.executor, self.export_settings)
                self.export_main_sequence_task = mvid_seq_task
                mvid_seq_task.initialize()
                self.tasks.append(mvid_seq_task)
            # Wait for prepare task to be done for actually starting.
            if self.prepare_task is None and mvid_seq_task.status == STATUS_READY:
                mvid_seq_task.start()
            if mvid_seq_task.status == STATUS_ACTIVE:
                self.status_message = "Exporting the main video sequence..."
            if mvid_seq_task.status == STATUS_DONE:
                ffmpeg_output = mvid_seq_task.future_result
                print(ffmpeg_output.stdout)
                print(ffmpeg_output.stderr)
                print("\n\n")
                self.tasks.remove(mvid_seq_task)
                self.export_main_sequence_task = None
                self.status_message = "Exporting the main video sequence... Done."

        if self.render_thumbnail_task is not None:
            thumbnail_task = self.render_thumbnail_task
            if thumbnail_task is ...:
                thumbnail_task = TaskExportingRenderFrame(
                    self, self.executor,
                    self.export_settings.blendfile_path,
                    self.export_settings.thumbnail_render_path_template,
                    "JPEG",  # FullHD PNG is too big for youtube thumbnails.
                    self.export_settings.thumbnail_frame
                )
                self.render_thumbnail_task = thumbnail_task
                thumbnail_task.initialize()
                self.tasks.append(thumbnail_task)
            # Wait for prepare task to be done for actually starting.
            if self.prepare_task is None and thumbnail_task.status == STATUS_READY:
                thumbnail_task.start()
            if thumbnail_task.status == STATUS_ACTIVE:
                self.status_message = "Rendering the thumbnail image..."
            if thumbnail_task.status == STATUS_DONE:
                blender_output = thumbnail_task.future_result
                print(blender_output.stdout)
                print(blender_output.stderr)
                print("\n\n")
                self.tasks.remove(thumbnail_task)
                self.render_thumbnail_task = None
                self.status_message = "Rendering the thumbnail image... Done."

        if self.render_titlecard_task is not None:
            tc_task = self.render_titlecard_task
            if tc_task is ...:
                tc_task = TaskExportingRenderFrame(
                    self, self.executor,
                    self.export_settings.blendfile_path,
                    self.export_settings.tc_render_path_template,
                    "PNG",
                    self.export_settings.tc_frame
                )
                self.render_titlecard_task = tc_task
                tc_task.initialize()
                self.tasks.append(tc_task)
            # Wait for prepare task to be done for actually starting.
            if self.prepare_task is None and tc_task.status == STATUS_READY:
                tc_task.start()
            if tc_task.status == STATUS_ACTIVE:
                self.status_message = "Rendering the titlecard image..."
            if tc_task.status == STATUS_DONE:
                blender_output = tc_task.future_result
                print(blender_output.stdout)
                print(blender_output.stderr)
                print("\n\n")
                self.tasks.remove(tc_task)
                self.render_titlecard_task = None
                self.status_message = "Rendering the titlecard image... Done."

        if self.export_titlecard_sequence_task is not None:
            tc_seq_task = self.export_titlecard_sequence_task
            if tc_seq_task is ...:
                tc_seq_task = TaskExportingTitleCardVideo(self, self.executor, self.export_settings)
                self.export_titlecard_sequence_task = tc_seq_task
                tc_seq_task.initialize()
                self.tasks.append(tc_seq_task)
            # Wait for titlecard to be rendered for actually starting.
            if self.render_titlecard_task is None and tc_seq_task.status == STATUS_READY:
                tc_seq_task.start()
            if tc_seq_task.status == STATUS_ACTIVE:
                self.status_message = "Exporting the titlecard video sequence..."
            if tc_seq_task.status == STATUS_DONE:
                ffmpeg_output = tc_seq_task.future_result
                print(ffmpeg_output.stdout)
                print(ffmpeg_output.stderr)
                print("\n\n")
                self.tasks.remove(tc_seq_task)
                self.export_titlecard_sequence_task = None
                self.status_message = "Exporting the titlecard video sequence... Done."

        if self.export_final_task is not None:
            final_task = self.export_final_task
            if final_task is ...:
                final_task = TaskExportingFinalVideo(self, self.executor, self.export_settings)
                self.export_final_task = final_task
                final_task.initialize()
                self.tasks.append(final_task)
            # Wait for all intermediary sequences export tasks to be done for actually starting final export.
            if (    self.export_titlecard_sequence_task is None and
                    self.export_main_sequence_task is None and
                    final_task.status == STATUS_READY):
                final_task.start()
            if final_task.status == STATUS_ACTIVE:
                self.status_message = "Exporting the final video..."
            if final_task.status == STATUS_DONE:
                ffmpeg_output = final_task.future_result
                print(ffmpeg_output.stdout)
                print(ffmpeg_output.stderr)
                print("\n\n")
                self.tasks.remove(final_task)
                self.export_final_task = None
                self.status_message = "Exporting the final video... Done."

        if self.export_user_validate_task is not None:
            validate_task = self.export_user_validate_task
            if validate_task is ...:
                validate_task = TaskExportingUserValidate(self, bl_talk, self.export_settings)
                self.export_user_validate_task = validate_task
                validate_task.initialize()
                self.tasks.append(validate_task)
            # Wait for all intermediary sequences export tasks to be done for actually starting final export.
            if self.export_final_task is None and validate_task.status == STATUS_READY:
                self.status_message = "Please validate the exported video."
                validate_task.start()
            if validate_task.status == STATUS_DONE:
                self.tasks.remove(validate_task)
                self.export_user_validate_task = None

        if not self.tasks:
            self.status_message = "Ready for uploading stage."

        super().update(bl_context, bl_talk)

    def next_stage_poll(self):
        if not super().next_stage_poll():
            return False
        # Do not allow moving to next stage if waiting for user action.
        if self.export_user_validate_task is not None:
            return False
        return True


class StageUploading(Stage):
    identifier = STAGE_UPLOADING

    def __init__(self, talk, executor, prev_stage):
        super().__init__(talk, executor, prev_stage)
        self.reset()

    def reset(self):
        # Handle backup of the final video.
        self.backup_task = ...
        # Handle uploading of the final video.
        self.upload_task = ...
        # Request user for validation of upload, before publication.
        self.user_validate_task = ...
        super().reset()

    def update(self, bl_context, bl_talk):
        if self.status == STATUS_PENDING:
            super().update(bl_context, bl_talk)
            return
        if IS_DEBUG:
            if bl_talk.skip_stage_upload:
                # Fully by-pass this stage.
                self.backup_task = None
                self.upload_task = None
                self.user_validate_task = None
                bl_talk.is_upload_stage_validation_required = False
                bl_talk.is_upload_stage_validated = True
                self.status_message = "Uploading stage skipped as requested, ready for final stage."

        if not bl_talk.export_backup_path:
            if self.backup_task is ...:
                self.backup_task = None
        if self.backup_task is not None:
            backup_task = self.backup_task
            if backup_task is ...:
                # NOTE: Could use TaskFileBackupAsync in theory, but non-blocking async tasks are not really tested
                #       currently, better not add that additional potential breaker for now. And copying to local
                #       backup NAS or something is expected at least as fast as youtube upload!
                backup_task = TaskFileCopyAsync(self, self.executor, bl_talk.export_path, bl_talk.export_backup_path, do_replace_dst=True)
                self.backup_task = backup_task
                backup_task.initialize()
                backup_task.start()
                self.tasks.append(backup_task)
            if backup_task.status == STATUS_ACTIVE:
                self.status_message = "Backing-up the final video..."
            if backup_task.status == STATUS_DONE:
                reports = backup_task.future_result
                print(reports)
                self.tasks.remove(backup_task)
                self.backup_task = None
                self.status_message = "Backing-up the final video... Done."

        if self.upload_task is not None:
            bl_talk.is_upload_stage_validation_required = False
            bl_talk.is_upload_stage_validated = False

            upload_task = self.upload_task
            if upload_task is ...:
                upload_task = TaskUploadingYoutube(self, self.executor, bl_talk)
                self.upload_task = upload_task
                upload_task.initialize()
                upload_task.start()
                self.tasks.append(upload_task)
            if upload_task.status == STATUS_ACTIVE:
                self.status_message = "Uploading the final video..."
            if upload_task.status == STATUS_DONE:
                upload_result = upload_task.future_result
                bl_talk.youtube_url = upload_result[0]
                bl_talk.youtube_edit_url = upload_result[1]
                bl_talk.is_upload_stage_validation_required = True
                self.tasks.remove(upload_task)
                self.upload_task = None
                self.status_message = "Uploading the final video... Done."

        if self.user_validate_task is not None:
            validate_task = self.user_validate_task
            if validate_task is ...:
                validate_task = TaskUploadingUserValidate(self, bl_talk)
                self.user_validate_task = validate_task
                validate_task.initialize()
                self.tasks.append(validate_task)
            # Wait for all intermediary sequences export tasks to be done for actually starting final export.
            if self.upload_task is None and validate_task.status == STATUS_READY:
                validate_task.start()
            if validate_task.status == STATUS_ACTIVE:
                self.status_message = "Please validate the exported video."
            if validate_task.status == STATUS_DONE:
                self.tasks.remove(validate_task)
                self.user_validate_task = None

        if not self.tasks:
            self.status_message = "Ready for final stage."

        super().update(bl_context, bl_talk)

    def next_stage_poll(self):
        if not super().next_stage_poll():
            return False
        # Do not allow moving to next stage if waiting for user action.
        if self.user_validate_task is not None:
            return False
        return True


class StageDone(Stage):
    identifier = STAGE_DONE

    def __init__(self, talk, executor, prev_stage):
        super().__init__(talk, executor, prev_stage)

    def start(self):
        super().start()
        self.reset()

    def reset(self):
        # Waits for all non-blocking async tasks from previous stages to be done.
        self.finalizing_task = ...
        # Handle cleanup of work directory on disk.
        self.cleanup_disk_task = ...
        # Handle cleanup of Blender data itself (VSE strips etc.).
        self.cleanup_blender_task = ...

    def update(self, bl_context, bl_talk):
        if self.status == STATUS_PENDING:
            super().update(bl_context, bl_talk)
            return
        if self.finalizing_task is not None:
            finalizing_task = self.finalizing_task
            if finalizing_task is ...:
                finalizing_task = TaskDoneFinalizing(self)
                self.finalizing_task = finalizing_task
                finalizing_task.initialize()
                finalizing_task.start()
                self.tasks.append(finalizing_task)
            if finalizing_task.status == STATUS_DONE:
                self.tasks.remove(finalizing_task)
                self.finalizing_task = None

        if self.cleanup_disk_task is not None:
            cleanup_disk_task = self.cleanup_disk_task
            if cleanup_disk_task is ...:
                cleanup_disk_task = TaskPathDeleteAsync(self, self.executor, bl_talk.work_dir)
                self.cleanup_disk_task = cleanup_disk_task
                cleanup_disk_task.initialize()
                self.tasks.append(cleanup_disk_task)
            # Wait for all non-blocking async tasks to be done before deleting local work data.
            if self.finalizing_task is None and cleanup_disk_task.status == STATUS_READY:
                cleanup_disk_task.start()
            if cleanup_disk_task.status == STATUS_DONE:
                self.tasks.remove(cleanup_disk_task)
                self.cleanup_disk_task = None

        if self.cleanup_blender_task is not None:
            cleanup_blender_task = self.cleanup_blender_task
            if cleanup_blender_task is ...:
                cleanup_blender_task = TaskDoneCleanupBlender(self, bl_context.scene, bl_talk)
                self.cleanup_blender_task = cleanup_blender_task
                cleanup_blender_task.initialize()
            # Wait for all non-blocking async tasks to be done before deleting local work data.
            if self.finalizing_task is None and cleanup_blender_task.status == STATUS_READY:
                # Immediate task.
                cleanup_blender_task.start()
                if cleanup_blender_task.status is not STATUS_DONE:
                    self.reports.add_error("Cleanup Blender task failed", cleanup_blender_task.reports)
                self.cleanup_blender_task = None

        if not self.tasks:
            self.finalize(bl_context, bl_talk, do_wait=False, do_update=False)
        super().update(bl_context, bl_talk)

    def next_stage_poll(self):
        return False


class Talk:
    """
    Internal management of a talk.
    """
    def __init__(self, bl_context, bl_talk, executor):
        self.reports = reports.ReportsHandler()
        self.uid = bl_talk.uid
        self._active_stage = 0
        self.stages = [StageUpcoming(self, executor, None)]
        self.stages.append(StageAcquiring(self, executor, self.stages[-1]))
        self.stages.append(StageProcessing(self, executor, self.stages[-1]))
        self.stages.append(StageExporting(self, executor, self.stages[-1]))
        self.stages.append(StageUploading(self, executor, self.stages[-1]))
        self.stages.append(StageDone(self, executor, self.stages[-1]))
        if bl_talk.stage != STAGE_UPCOMING:
            bl_talk_target_stage = bl_talk.stage
            bl_talk.stage = STAGE_UPCOMING
            while bl_talk.stage not in {bl_talk_target_stage, STAGE_DONE}:
                if not self.next_stage(bl_context, bl_talk):
                    break;
                self.update(bl_context, bl_talk)

    def next_stage_poll(self, bl_context, bl_talk):
        stage = self.active_stage
        assert bl_talk.stage == stage.identifier

        return stage.next_stage_poll()

    def next_stage(self, bl_context, bl_talk):
        assert bl_talk.stage == self.active_stage.identifier
        if not self.next_stage_poll(bl_context, bl_talk):
            return False

        stage = self.active_stage
        if stage.status == STATUS_ACTIVE:
            stage.finalize(bl_context, bl_talk, do_wait=False)

        if stage.status in {STATUS_FINALIZING, STATUS_DONE}:
            self._active_stage += 1
            stage = self.active_stage
            stage.start()
            bl_talk.stage = stage.identifier
            return True
        return False

    def prev_stage_poll(self, bl_context, bl_talk):
        stage = self.active_stage
        assert bl_talk.stage == stage.identifier
        return stage.prev_stage_poll()

    def prev_stage(self, bl_context, bl_talk):
        assert bl_talk.stage == self.active_stage.identifier
        if not self.prev_stage_poll(bl_context, bl_talk):
            return False

        stage = self.active_stage
        if stage.status in {STATUS_ERROR, STATUS_ACTIVE, STATUS_PENDING, STATUS_FINALIZING, STATUS_DONE}:
            stage.reset()

        self._active_stage -= 1
        stage = self.active_stage
        stage.reset()
        stage.start()
        bl_talk.stage = stage.identifier
        return True

    def update(self, bl_context, bl_talk):
        for stage in self.stages[:self._active_stage + 1]:
            stage.update(bl_context, bl_talk)

    def reset(self):
        for stage in self.stages[self._active_stage - len(self.stages)::-1]:
            stage.reset()
        self._active_stage = 0

    @property
    def active_stage(self):
        return self.stages[self._active_stage]

    @property
    def status_message(self):
        return self.active_stage.status_message


class TalksManager:
    """
    Manage active talks.
    
    Handles the logic to switch between stages, and ensures blender-side of talks data is up-to-date, valid, etc.
    
    NOTE: This is a singleton, which makes it persistent and easy to access from anywhere whithin Blender code (operators).
    """
    def __new__(cls):
        do_reset_timestamps = False
        if getattr(cls, 'instance', None) is None:
            cls.instance = super(TalksManager, cls).__new__(cls)
            cls.instance.active_talks = {}
            cls.instance.executor = concurrent.futures.ProcessPoolExecutor()
            do_reset_timestamps = True
        elif cls.instance.executor is None:
            cls.instance.executor = concurrent.futures.ProcessPoolExecutor()
            do_reset_timestamps = True

        # Timestamps of last update of the (read-only) reference data for talks, and persistent dump of talks processing.
        if do_reset_timestamps:
            cls.instance.last_update_reference_data = 0.0
            cls.instance.last_update_persistent_data = 0.0

        return cls.instance

    def clear(self):
        for talk in self.active_talks.values():
            talk.reset()
        self.active_talks.clear()
        # This seems to be the only way to ensure all futures of a given executor are cancelled... 
        self.executor.shutdown(wait=False, cancel_futures=True)
        self.executor = None

    def talk_is_active(self, bl_talk):
        return bl_talk.uid in self.active_talks

    def talk_init(self, bl_context, bl_talk):
        if self.executor is None:
            self.executor = concurrent.futures.ProcessPoolExecutor()
        assert bl_talk.uid not in self.active_talks
        self.active_talks[bl_talk.uid] = Talk(bl_context, bl_talk, self.executor)

    def talk_reset(self, bl_talk):
        assert bl_talk.uid in self.active_talks
        self.active_talks[bl_talk.uid].reset()

    def talk_delete(self, bl_talk):
        assert bl_talk.uid in self.active_talks
        self.active_talks[bl_talk.uid].reset()
        del self.active_talks[bl_talk.uid]

    def talk_update(self, bl_context, bl_talk):
        """
        Update Blender-side talk data to reflect current status of the given talk, and vice-versa.
        
        Is expected to always leave Blender-side talk data in a valid, consistent, and resumable state.
        """
        assert bl_talk.uid in self.active_talks
        talk = self.active_talks[bl_talk.uid]
        talk.update(bl_context, bl_talk)
        talk_status = talk.active_stage.status
        # Remove (de-activate) a talk that is fully done.
        if talk_status == STATUS_DONE and talk.active_stage.identifier == STAGE_DONE:
            del self.active_talks[bl_talk.uid]
        return talk_status

    def talk_stage(self, bl_talk):
        assert bl_talk.uid in self.active_talks
        return self.active_talks[bl_talk.uid].active_stage.identifier

    def talk_status(self, bl_talk):
        assert bl_talk.uid in self.active_talks
        return self.active_talks[bl_talk.uid].active_stage.status

    def talk_status_message(self, bl_talk):
        assert bl_talk.uid in self.active_talks
        return self.active_talks[bl_talk.uid].status_message

    def talk_next_stage_poll(self, bl_context, bl_talk):
        assert bl_talk.uid in self.active_talks
        return self.active_talks[bl_talk.uid].next_stage_poll(bl_context, bl_talk)

    def talk_prev_stage_poll(self, bl_context, bl_talk):
        assert bl_talk.uid in self.active_talks
        return self.active_talks[bl_talk.uid].prev_stage_poll(bl_context, bl_talk)

    def talk_next_stage(self, bl_context, bl_talk):
        assert bl_talk.uid in self.active_talks
        return self.active_talks[bl_talk.uid].next_stage(bl_context, bl_talk)

    def talk_prev_stage(self, bl_context, bl_talk):
        assert bl_talk.uid in self.active_talks
        talk = self.active_talks[bl_talk.uid]
        if talk.prev_stage(bl_context, bl_talk):
            # Remove (de-activate) a talk that is brought back to 'UPCOMING' stage.
            if talk.active_stage.identifier == STAGE_UPCOMING:
                del self.active_talks[bl_talk.uid]
            return True
        return False


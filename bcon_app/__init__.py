# SPDX-License-Identifier: GPL-2.0-or-later

# NOTE: Unused by the 'blender app' system...
bl_info = {
    "name": "BCon Video Processing",
    "author": "Bastien Montagne",
    "version": (0, 0, 1),
    "blender": (4, 0, 0),
    "location": "VSE",
    "description": "All-in-one processing tool for encoding and uploading of the Blender Conference videos",
    "warning": "",
    "doc_url": "{BLENDER_MANUAL_URL}/addons/vse/bcon_video",
    "support": 'COMMUNITY',
    "category": "VSE",
}


from . import (
    bl_data,
    bl_ops,
    bl_ui,
    # NOTE: `upload_youtube` and non-directly linked to Blender modules are purposedly ignored here.
)
if "bpy" in locals():
    import importlib
    if "reports" in locals():
        importlib.reload(reports)
    if "stages" in locals():
        importlib.reload(stages)
    if "bl_data" in locals():
        importlib.reload(bl_data)
    if "ops" in locals():
        importlib.reload(ops)
    if "ui" in locals():
        importlib.reload(ui)


import bpy
from bpy.props import (
        StringProperty,
        BoolProperty,
        FloatProperty,
        IntProperty,
        EnumProperty,
        CollectionProperty,
        PointerProperty,
        )


register_classes = bl_data.register_classes + bl_ops.register_classes + bl_ui.register_classes


def register():
    for cls in register_classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.bcon_video_data = PointerProperty(type=bl_data.BConVideoData)

    bpy.app.handlers.load_post.append(bl_data.load_post_handler)


def unregister():
    bpy.app.handlers.load_post.remove(bl_data.load_post_handler)

    bcon_vp_timer = BConVP_Timer()
    if bpy.app.timers.is_registered(bcon_vp_timer):
        bpy.app.timers.unregister(bcon_vp_timer)
    del bpy.types.Scene.bcon_video_data

    for cls in register_classes[-1:]:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()

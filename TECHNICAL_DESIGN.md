# Technical Design

This page documents the general design behind the `BCon App` Blender application. The target of this tool is to process raw videos recorded during the Blender Conferences for (most) talks, and upload them on youtube.

## Overview

Process is implemented as a linear set of *stages*. Each talk goes through the same set of stages. Several talks can be processed independently at the same time.

All active talks are managed by a single instance (singleton) of the *Talk Manager*, which keeps track during runtime of their statuses, handle moving them to the next or previous stage, etc. Talks that are not known to the talk manager are considered *inactive*.

Each stage contains a logical coherent set of *tasks*. All tasks of a stage must be started before the talk can move to the next stage.
While typically tasks are specific to a stage, some simple one can be used at several stages, e.g. file operations like copying.

A task can be either *synchronous* or *ascynchronous*, and for the latter, stage-*blocking* of stage-*non-blocking*:
 * *Synchronous*: it is blocking and happen in the foreground. Typically, either a very fast task, or a task that requires interraction with Blender data. It is always *blocking* by definition.
 * *Asynchronous*: it is ran in the background and is not blocking the Blender UI.
   - *Blocking*: The stage cannot be considered as done before the task is done.
   - *Non-blocking*: the stage can be considered as done even if the task is still active.

There are two sources of persitent data:
 * *Reference* data defines the talks themselves and their meta-data. it comes from the BCon website, as a downloaded JSon file. This data is never modified by the app.
 * *Persistent* data stores the current status of all talks in the BCon App. It is a JSon file, typically stored locally in the worker machine, and periodically updated while the BCon App is running.

Runtime Data is stored:
  * As Blender data in the Scene data-block, for UI and Operators access.
  * As part of the the *Talk Manager*.
The talk manager is also responsible to ensure synchronization with the Blender data.

A timer is used as 'even loop' handler when some talks are active.

------------------

Notes:
* [Original design task](https://projects.blender.org/infrastructure/conference-video-processing/issues/1).
* [BCon23 follow-up task](https://projects.blender.org/infrastructure/conference-video-processing/issues/2).